package net.skoumal.joogar2.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Pavel Salva
 */


@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.SOURCE)
public @interface Index {
}
