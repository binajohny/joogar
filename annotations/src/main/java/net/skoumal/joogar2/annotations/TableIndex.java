package net.skoumal.joogar2.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Pavel Salva
 */


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface TableIndex {
    String[] columns();
    boolean unique() default false;

}
