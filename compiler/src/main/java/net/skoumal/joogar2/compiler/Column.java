package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * Created by Jan Bína
 */

public abstract class Column {

    private String name;
    private String variableName;
    private TypeName typeName;
    private VariableElement variable;
    private ExecutableElement getter;
    private ExecutableElement setter;
    private boolean primaryKey;
    private boolean unique;
    private boolean nonNull;
    private boolean index;

    public Column(String name, TypeName typeName, VariableElement variable, ExecutableElement getter,
                  ExecutableElement setter, boolean primaryKey, boolean unique, boolean nonNull, boolean index) {
        this.name = name;
        this.variableName = variable.getSimpleName().toString();
        this.typeName = typeName;
        this.variable = variable;
        this.getter = getter;
        this.setter = setter;
        this.primaryKey = primaryKey;
        this.unique = unique;
        this.nonNull = nonNull;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getVariableName() {
        return variableName;
    }

    public TypeName getTypeName() {
        return typeName;
    }

    public VariableElement getVariable() {
        return variable;
    }

    public ExecutableElement getGetter() {
        return getter;
    }

    public ExecutableElement getSetter() {
        return setter;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public boolean isUnique() {
        return unique;
    }

    public boolean isNonNull(){
        return nonNull;
    }

    public boolean isIndex(){
        return index;
    }


    public boolean isIntegerType() {
        return typeName.equals(TypeName.INT) || typeName.equals(TypeName.INT.box()) ||
                typeName.equals(TypeName.LONG) || typeName.equals(TypeName.LONG.box());
    }

    public String getSqliteType() {
        if (typeName.equals(TypeName.get(byte[].class)) || typeName.equals(TypeName.get(Byte[].class))) {
            return "blob";
        }
        if (typeName.equals(TypeName.DOUBLE) || typeName.equals(TypeName.DOUBLE.box()) ||
            typeName.equals(TypeName.FLOAT) || typeName.equals(TypeName.FLOAT.box())) {
            return "real";
        }
        if (typeName.equals(TypeName.INT) || typeName.equals(TypeName.INT.box()) ||
            typeName.equals(TypeName.LONG) || typeName.equals(TypeName.LONG.box()) ||
            typeName.equals(TypeName.SHORT) || typeName.equals(TypeName.SHORT.box()) ||
            typeName.equals(TypeName.CHAR) || typeName.equals(TypeName.CHAR.box()) ||
            typeName.equals(TypeName.BOOLEAN) || typeName.equals(TypeName.BOOLEAN.box()) ||
            typeName.equals(TypeName.BYTE) || typeName.equals(TypeName.BYTE.box())) {
            return "integer";
        }
        if (typeName.equals(TypeName.get(String.class)) ||
            typeName.equals(TypeName.CHAR) || typeName.equals(TypeName.CHAR.box())) {
            return "text";
        }
        throw new IllegalStateException();
    }

    public CodeBlock getDatabaseResultMethod(String resultName, int columnIndex) {
        String format;
        if (typeName.equals(TypeName.get(byte[].class))) {
            format = "$L.getBlob($L)";
        } else if(typeName.equals(TypeName.get(Byte[].class))) {
            return CodeBlock.of("$T.toObject($L.getBlob($L))", JoogarClasses.utils, resultName, columnIndex);
        } else if (typeName.equals(TypeName.DOUBLE) || typeName.equals(TypeName.DOUBLE.box())) {
            format = "$L.getDouble($L)";
        } else if (typeName.equals(TypeName.FLOAT) || typeName.equals(TypeName.FLOAT.box())) {
            format = "$L.getFloat($L)";
        } else if (typeName.equals(TypeName.INT) || typeName.equals(TypeName.INT.box())) {
            format = "$L.getInt($L)";
        } else if (typeName.equals(TypeName.LONG) || typeName.equals(TypeName.LONG.box())) {
            format = "$L.getLong($L)";
        } else if (typeName.equals(TypeName.SHORT) || typeName.equals(TypeName.SHORT.box())) {
            format = "(short) $L.getInt($L)";
        } else if (typeName.equals(TypeName.get(String.class))) {
            format = "$L.getString($L)";
        } else if (typeName.equals(TypeName.BOOLEAN) || typeName.equals(TypeName.BOOLEAN.box())) {
            format = "$L.getInt($L) == 1";
        } else if (typeName.equals(TypeName.CHAR) || typeName.equals(TypeName.CHAR.box())) {
            format = "$L.getString($L).charAt(0)";
        } else if (typeName.equals(TypeName.BYTE) || typeName.equals(TypeName.BYTE.box())) {
            format = "(byte) $L.getInt($L)";
        } else {
            throw new AssertionError("");
        }

        return CodeBlock.of(format, resultName, columnIndex);
    }

    public CodeBlock getBinderMethodName(String accessor, String statement, int index) {
        String format;
        if (typeName.equals(TypeName.get(byte[].class))) {
            format = "$L.bindBlob($L, $L)";
        } else if(typeName.equals(TypeName.get(Byte[].class))) {
            return CodeBlock.of("$L.bindBlob($L, $T.toPrimitive($L))", statement, index, JoogarClasses.utils, accessor);
        } else if (typeName.equals(TypeName.DOUBLE) || typeName.equals(TypeName.DOUBLE.box())
                || typeName.equals(TypeName.FLOAT) || typeName.equals(TypeName.FLOAT.box())) {
            format = "$L.bindDouble($L, $L)";
        } else if (typeName.equals(TypeName.INT) || typeName.equals(TypeName.INT.box())
                || typeName.equals(TypeName.LONG) || typeName.equals(TypeName.LONG.box())
                || typeName.equals(TypeName.SHORT) || typeName.equals(TypeName.SHORT.box())
                || typeName.equals(TypeName.BYTE) || typeName.equals(TypeName.BYTE.box())) {
            format = "$L.bindLong($L, $L)";
        } else if (typeName.equals(TypeName.get(String.class))) {
            format = "$L.bindString($L, $L)";
        } else if (typeName.equals(TypeName.BOOLEAN) || typeName.equals(TypeName.BOOLEAN.box())) {
            format = "$L.bindLong($L, $L ? 1 : 0)";
        } else if (typeName.equals(TypeName.CHAR) || typeName.equals(TypeName.CHAR.box())) {
            format = "$L.bindString($L, String.valueOf($L))";
        } else {
            throw new AssertionError("");
        }

        return CodeBlock.of(format, statement, index, accessor);
    }

    public abstract CodeBlock fromDbResult(String entityName, String resultName, int columnIndex);
    public abstract CodeBlock bindStatement(String accessor, String statement, int index, boolean excludeIds);
    public abstract CodeBlock createColumnBlock(String entity);
    public abstract FieldSpec createTableHelperProperty(TypeName typeName);
    public abstract String getAccessor();
    public abstract String getMutator();
}
