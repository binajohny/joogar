package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import net.skoumal.joogar2.annotations.TableIndex;
import net.skoumal.joogar2.annotations.TableIndexes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

/**
 * Created by Jan Bína & Pavel Salva
 */

public class TableEntity {
    private String name;
    private String entityName;
    private TypeName typeName;
    private TypeElement typeElement;
    private Map<String, Column> columns;
    private List<Column> primaryKeys;
    private ClassName helperClass;
    private TableIndex tableIndex;
    private TableIndexes tableIndexes;

    //Jan Bína
    public TableEntity(String name, String entityName, TypeName typeName, TypeElement typeElement, TableIndex tableIndex, TableIndexes tableIndexes) {
        this.name = name;
        this.entityName = entityName;
        this.typeName = typeName;
        this.typeElement = typeElement;
        this.columns = new HashMap<>();
        this.primaryKeys = new ArrayList<>();
        this.tableIndex = tableIndex;
        this.tableIndexes = tableIndexes;

        this.helperClass = ClassName.get(
                ((PackageElement) typeElement.getEnclosingElement()).getQualifiedName().toString(),
                entityName + "Table");
    }

    //Jan Bína
    public boolean addColumn(String name, Column column) {
        if (columns.get(name) != null) {
            return false;
        }
        columns.put(name, column);
        if (column.isPrimaryKey()) {
            primaryKeys.add(column);
        }
        return true;
    }

    public String getName() {
        return name;
    }

    public String getEntityName() {
        return entityName;
    }

    public TypeName getTypeName() {
        return typeName;
    }

    public TypeElement getTypeElement() {
        return typeElement;
    }

    public Collection<Column> getColumns() {
        return columns.values();
    }

    public List<Column> getPrimaryKeys() {
        return primaryKeys;
    }

    public ClassName getHelperClass() {
        return helperClass;
    }

    //Jan Bína
    public JavaFile createTableHelperFile(String dbName) {
        TypeSpec.Builder clazz = TypeSpec.classBuilder(helperClass.simpleName())
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL);

        MethodSpec getDatabaseName = MethodSpec.methodBuilder("getDatabaseName")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(String.class)
                .addStatement("return $S", dbName)
                .build();

        clazz.addMethod(getDatabaseName);

        MethodSpec getTableName = MethodSpec.methodBuilder("getName")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(String.class)
                .addStatement("return $S", name)
                .build();

        clazz.addMethod(getTableName);


        CodeBlock.Builder columnsInitializer = CodeBlock.builder()
                .add("new $T[]{\n", ClassName.get(String.class))
                .indent();

        for (Column column : getColumns()) {
            columnsInitializer.add("$S,\n", column.getName());
        }
        columnsInitializer.unindent();
        columnsInitializer.add("}");

        FieldSpec columns = FieldSpec.builder(ArrayTypeName.of(ClassName.get(String.class)),
                "columns", Modifier.PUBLIC, Modifier.STATIC)
                .initializer(columnsInitializer.build())
                .build();

        clazz.addField(columns);

        FieldSpec statement = FieldSpec.builder(JoogarClasses.androidSQLiteStatement,
                "insertStatement", Modifier.PRIVATE, Modifier.STATIC)
                .initializer("null")
                .build();

        clazz.addField(statement);

        statement = FieldSpec.builder(JoogarClasses.androidSQLiteStatement,
                "insertOrReplaceStatement", Modifier.PRIVATE, Modifier.STATIC)
                .initializer("null")
                .build();

        clazz.addField(statement);

        statement = FieldSpec.builder(JoogarClasses.androidSQLiteStatement,
                "updateStatement", Modifier.PRIVATE, Modifier.STATIC)
                .initializer("null")
                .build();

        clazz.addField(statement);

        statement = FieldSpec.builder(JoogarClasses.androidSQLiteStatement,
                "deleteStatement", Modifier.PRIVATE, Modifier.STATIC)
                .initializer("null")
                .build();

        clazz.addField(statement);

        String columnNames = "(";
        String values = "(";
        int i = 0;
        for (Column column : getColumns()) {
            if (i > 0) {
                columnNames += ", ";
                values += ", ";
            }
            columnNames += column.getName();
            values += "?";
            i++;
        }
        columnNames += ")";
        values += ")";

        String insertStmt = "INSERT INTO " + name + " " + columnNames + " VALUES " + values;

        MethodSpec.Builder getInsertStatement = MethodSpec.methodBuilder("getInsertStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .returns(JoogarClasses.androidSQLiteStatement)
                .beginControlFlow("if (insertStatement == null)")
                .addStatement("insertStatement = $L.compileStatement($S)",
                        "getDatabase()", insertStmt)
                .endControlFlow()
                .addStatement("return insertStatement");

        clazz.addMethod(getInsertStatement.build());

        String insertReplaceStmt = "INSERT OR REPLACE INTO " + name + " " + columnNames + " VALUES " + values;

        MethodSpec.Builder getInsertReplaceStatement = MethodSpec.methodBuilder("getInsertOrReplaceStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .returns(JoogarClasses.androidSQLiteStatement)
                .beginControlFlow("if (insertOrReplaceStatement == null)")
                .addStatement("insertOrReplaceStatement = $L.compileStatement($S)",
                        "getDatabase()", insertReplaceStmt)
                .endControlFlow()
                .addStatement("return insertOrReplaceStatement");

        clazz.addMethod(getInsertReplaceStatement.build());

        String where = "";
        i = 0;
        for (Column column : getPrimaryKeys()) {
            if (i > 0) {
                where += " AND ";
            }
            where += column.getName() + " = ?";
            i++;
        }

        String columnNamesWithValues = "";
        i = 0;
        for (Column column : getColumns()) {
            if (!column.isPrimaryKey()) {
                if (i > 0) {
                    columnNamesWithValues += ", ";
                }
                columnNamesWithValues += column.getName() + " = ?";
                i++;
            }
        }

        String updateStmt = "UPDATE " + name + " SET " + columnNamesWithValues + " WHERE " + where;
        String deleteStmt = "DELETE FROM " + name + " WHERE " + where;

        MethodSpec.Builder getUpdateStatement = MethodSpec.methodBuilder("getUpdateStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .returns(JoogarClasses.androidSQLiteStatement)
                .beginControlFlow("if (updateStatement == null)")
                .addStatement("updateStatement = $L.compileStatement($S)",
                        "getDatabase()", updateStmt)
                .endControlFlow()
                .addStatement("return updateStatement");

        clazz.addMethod(getUpdateStatement.build());

        MethodSpec.Builder getDeleteStatement = MethodSpec.methodBuilder("getDeleteStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .returns(JoogarClasses.androidSQLiteStatement)
                .beginControlFlow("if (deleteStatement == null)")
                .addStatement("deleteStatement = $L.compileStatement($S)",
                        "getDatabase()", deleteStmt)
                .endControlFlow()
                .addStatement("return deleteStatement");

        clazz.addMethod(getDeleteStatement.build());

        for (Column column : getColumns()) {
            clazz.addField(column.createTableHelperProperty(JoogarClasses.columnProperty(typeName)));
        }

        clazz.addField(getEntityBuilderField());
        clazz.addMethods(createStatementBinders());

        MethodSpec.Builder close = MethodSpec.methodBuilder("close")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .beginControlFlow("if (insertStatement != null)")
                .addStatement("insertStatement.close()")
                .addStatement("insertStatement = null")
                .endControlFlow()
                .beginControlFlow("if (insertOrReplaceStatement != null)")
                .addStatement("insertOrReplaceStatement.close()")
                .addStatement("insertOrReplaceStatement = null")
                .endControlFlow()
                .beginControlFlow("if (updateStatement != null)")
                .addStatement("updateStatement.close()")
                .addStatement("updateStatement = null")
                .endControlFlow()
                .beginControlFlow("if (deleteStatement != null)")
                .addStatement("deleteStatement.close()")
                .addStatement("deleteStatement = null")
                .endControlFlow();

        MethodSpec.Builder getWhere = MethodSpec.methodBuilder("where")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(JoogarClasses.entityWhere(entityName))
                .addStatement("return new $T()", JoogarClasses.entityWhere(entityName));

        MethodSpec.Builder getDatabase = MethodSpec.methodBuilder("getDatabase")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(JoogarClasses.joogarDatabase)
                .addStatement("return $T.getInstance().getDB($S)", JoogarClasses.joogar, dbName);

        clazz.addMethods(createSaveMethods());
        clazz.addMethods(createDeleteMethods());
        clazz.addMethods(createFindMethods());

        clazz.addMethod(getWhere.build());
        clazz.addMethod(getDatabase.build());
        clazz.addMethod(close.build());

        return JavaFile.builder(
                helperClass.packageName(),
                clazz.build()
        ).skipJavaLangImports(true).indent("    ").build();
    }

    //Jan Bína
    private FieldSpec getEntityBuilderField() {
        TypeName entityBuilder = JoogarClasses.entityBuilder(typeName);

        String resultName = "result";
        String entityName = Utils.lowerCaseFirst(this.entityName);

        CodeBlock.Builder initializer = CodeBlock.builder()
                .add("new $T() {\n", entityBuilder)
                .indent()
                .add("@$T\n", ClassName.get(Override.class))
                .add("public $T get($T $L) {\n", typeName, JoogarClasses.joogarDatabaseResult, resultName)
                .indent()
                .addStatement("$1T $2L = new $1T()", typeName, entityName);

        int i = 0;
        for (Column column : getColumns()) {
            initializer.add(column.fromDbResult(entityName, resultName, i++));
        }

        initializer.addStatement("return $L", entityName)
                .unindent()
                .add("}\n")
                .unindent()
                .add("}");

        FieldSpec.Builder builder = FieldSpec.builder(
                entityBuilder,
                "entityBuilder",
                Modifier.PRIVATE, Modifier.STATIC
        ).initializer(initializer.build());

        return builder.build();
    }

    //Jan Bína
    private List<MethodSpec> createStatementBinders() {
        List<MethodSpec> methods = new ArrayList<>();
        MethodSpec.Builder builder;
        int i;

        builder = MethodSpec.methodBuilder("bindInsertStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .addParameter(typeName, "entity")
                .addParameter(JoogarClasses.androidSQLiteStatement, "statement")
                .addStatement("statement.clearBindings()");

        i = 1;
        for (Column column : getColumns()) {
            builder.addCode(column.bindStatement("entity." + column.getAccessor(), "statement", i++, primaryKeys.size() == 1));
        }

        methods.add(builder.build());

        builder = MethodSpec.methodBuilder("bindUpdateStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .addParameter(typeName, "entity")
                .addParameter(JoogarClasses.androidSQLiteStatement, "statement")
                .addStatement("statement.clearBindings()");

        i = 1;
        for (Column column : getColumns()) {
            if (!column.isPrimaryKey()) {
                builder.addCode(column.bindStatement("entity." + column.getAccessor(), "statement", i++, true));
            }
        }
        for (Column column : getPrimaryKeys()) {
            builder.addCode(column.bindStatement("entity." + column.getAccessor(), "statement", i++, false));
        }

        methods.add(builder.build());

        builder = MethodSpec.methodBuilder("bindDeleteStatement")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                .addStatement("statement.clearBindings()");

        i = 1;
        for (Column column : getPrimaryKeys()) {
            builder.addParameter(column.getTypeName(), column.getVariableName());
            builder.addCode(column.bindStatement(column.getVariableName(), "statement", i++, false));
        }


        builder.addParameter(JoogarClasses.androidSQLiteStatement, "statement");

        methods.add(builder.build());

        return methods;
    }

    //Jan Bína
    public List<MethodSpec> createFindMethods() {
        List<MethodSpec> result = new ArrayList<>();

        // general find
        TypeName findStatement = JoogarClasses.findStatement(typeName, JoogarClasses.entityWhere(entityName));
        MethodSpec.Builder builder = MethodSpec.methodBuilder("find")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(findStatement)
                .addStatement("return new $T($L, $L, $S, $L)", findStatement,
                        "entityBuilder", "getDatabase()", name, "columns");

        result.add(builder.build());

        String base = "findBy";

        // find by primary key
        String pkMethodName;
        if (getPrimaryKeys().size() == 1) {
            pkMethodName = Utils.upperCaseFirst(getPrimaryKeys().get(0).getVariableName());
        } else {
            pkMethodName = "PrimaryKey";
        }

        builder = MethodSpec.methodBuilder(base + pkMethodName)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(typeName);

        StringBuilder where = new StringBuilder();
        StringBuilder whereArgs = new StringBuilder();
        for (int i = 0; i < getPrimaryKeys().size(); i++) {
            Column column = getPrimaryKeys().get(i);
            if (i > 0) {
                where.append(" AND ");
                whereArgs.append(", ");
            }
            where.append(column.getName())
                    .append(" = ?");
            whereArgs.append("String.valueOf(").append(column.getVariableName()).append(")");

            builder.addParameter(column.getTypeName(), column.getVariableName());
        }

        builder.addStatement("$T where = $S", ClassName.get(String.class), where.toString());
        builder.addStatement("$1T[] whereArgs = new $1T[]{$2L}", ClassName.get(String.class), whereArgs.toString());
        builder.addStatement("$T cursor = $L.query($S, $L, $L, $L, null, null, null, $S).toJoogarCursor($L)",
                JoogarClasses.joogarCursor(typeName), "getDatabase()", name, "columns", "where", "whereArgs", 1, "entityBuilder");
        builder.addStatement("$T result = cursor.next()", typeName);
        builder.addStatement("cursor.close()");
        builder.addStatement("return result");

        result.add(builder.build());

        //count with where
        builder = MethodSpec.methodBuilder("count")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(int.class)
                .addParameter(JoogarClasses.entityWhere(entityName), "where")
                .addStatement("$T result = $L.query($S, new $T[]{$S}, $L, $L, null, null, null)",
                        JoogarClasses.joogarDatabaseResult, "getDatabase()", name,
                        String.class, "COUNT(*)", "where.getWhere()", "where.getArgs()")
                .addStatement("result.next()")
                .addStatement("int count = result.getInt(0)")
                .addStatement("result.close()")
                .addStatement("return count");

        result.add(builder.build());

        //count all
        builder = MethodSpec.methodBuilder("countAll")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(int.class)
                .addStatement("$T result = $L.query($S, new $T[]{$S}, null, null, null, null, null)",
                        JoogarClasses.joogarDatabaseResult, "getDatabase()", name,
                        String.class, "COUNT(*)")
                .addStatement("result.next()")
                .addStatement("int count = result.getInt(0)")
                .addStatement("result.close()")
                .addStatement("return count");

        result.add(builder.build());

        return result;
    }

    //Jan Bína
    public List<MethodSpec> createDeleteMethods() {
        List<MethodSpec> result = new ArrayList<>();

        // delete by primary key
        String deleteByPk = "deleteBy";
        if (getPrimaryKeys().size() == 1) {
            deleteByPk += Utils.upperCaseFirst(getPrimaryKeys().get(0).getVariableName());
        } else {
            deleteByPk += "PrimaryKey";
        }

        MethodSpec.Builder builder = MethodSpec.methodBuilder(deleteByPk)
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(boolean.class)
                .addStatement("$T statement = getDeleteStatement()", JoogarClasses.androidSQLiteStatement);

        StringBuilder bindParams = new StringBuilder();
        for (Column column : getPrimaryKeys()) {
            bindParams.append(column.getVariableName()).append(", ");

            builder.addParameter(column.getTypeName(), column.getVariableName());
        }

        builder.addStatement("bindDeleteStatement($L$L)", bindParams, "statement")
                .addStatement("return statement.executeUpdateDelete() == 1");

        result.add(builder.build());

        // delete object
        builder = MethodSpec.methodBuilder("delete")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(typeName, Utils.lowerCaseFirst(entityName))
                .returns(boolean.class);

        StringBuilder params = new StringBuilder();
        for (int i = 0; i < getPrimaryKeys().size(); i++) {
            Column column = getPrimaryKeys().get(i);
            if (i > 0) {
                params.append(", ");
            }
            params.append(Utils.lowerCaseFirst(entityName)).append(".").append(column.getAccessor());
        }

        builder.addStatement("return $L($L)", deleteByPk, params.toString());

        result.add(builder.build());

        // delete in transaction
        builder = MethodSpec.methodBuilder("deleteInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(ParameterizedTypeName.get(ClassName.get(Collection.class), typeName), "objects", Modifier.FINAL)
                .returns(int.class)
                .addStatement("int count = 0")
                .addStatement("$L.beginTransaction()", "getDatabase()")
                .beginControlFlow("try")
                .beginControlFlow("for ($T object : objects)", typeName)
                .addStatement("count += delete(object) ? 1 : 0")
                .endControlFlow()
                .addStatement("$L.setTransactionSuccessful()", "getDatabase()")
                .nextControlFlow("finally")
                .addStatement("$L.endTransaction()", "getDatabase()")
                .endControlFlow()
                .addStatement("return count");


        result.add(builder.build());

        builder = MethodSpec.methodBuilder("deleteInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(typeName, "first", Modifier.FINAL)
                .addParameter(ArrayTypeName.of(typeName), "others", Modifier.FINAL)
                .returns(int.class)
                .varargs()
                .addStatement("int count = 0")
                .addStatement("$L.beginTransaction()", "getDatabase()")
                .beginControlFlow("try")
                .addStatement("count += delete(first) ? 1 : 0")
                .beginControlFlow("for ($T object : others)", typeName)
                .addStatement("count += delete(object) ? 1 : 0")
                .endControlFlow()
                .addStatement("$L.setTransactionSuccessful()", "getDatabase()")
                .nextControlFlow("finally")
                .addStatement("$L.endTransaction()", "getDatabase()")
                .endControlFlow()
                .addStatement("return count");

        result.add(builder.build());

        //delete with where
        builder = MethodSpec.methodBuilder("delete")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(int.class)
                .addParameter(JoogarClasses.entityWhere(entityName), "where")
                .addStatement("return $L.delete($S, $L, $L)",
                        "getDatabase()", name, "where.getWhere()", "where.getArgs()");

        result.add(builder.build());

        //delete all
        builder = MethodSpec.methodBuilder("deleteAll")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(int.class)
                .addStatement("return $L.delete($S, $L, $L)",
                        "getDatabase()", name, "null", "null");

        result.add(builder.build());

        return result;
    }

    //Jan Bína
    public List<MethodSpec> createSaveMethods() {
        List<MethodSpec> methods = new ArrayList<>();

        String entityNameLC = Utils.lowerCaseFirst(entityName);

        // Simple save
        MethodSpec.Builder builder = MethodSpec.methodBuilder("save")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(typeName, entityNameLC)
                .addStatement("$T statement = getInsertOrReplaceStatement()", JoogarClasses.androidSQLiteStatement)
                .addStatement("bindInsertStatement($L, statement)", entityNameLC)
                .addStatement("long id = statement.executeInsert()");

        if (getPrimaryKeys().size() == 1) {
            Column id = getPrimaryKeys().get(0);
            if (id.isIntegerType()) {
                String idName;
                if (id.getTypeName().equals(TypeName.INT) || id.getTypeName().equals(TypeName.INT.box())) {
                    idName = "(int) id";
                } else {
                    idName = "id";
                }
                builder.addStatement("$L." + id.getMutator(), entityNameLC, idName);
            }
        }

        builder.addStatement("return id");

        methods.add(builder.build());

        // Save in transaction
        builder = MethodSpec.methodBuilder("saveInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(ParameterizedTypeName.get(ClassName.get(Collection.class), typeName), "objects", Modifier.FINAL)
                .addStatement("getDatabase().beginTransaction()")
                .beginControlFlow("try")
                .beginControlFlow("for ($T object : objects)", typeName)
                .addStatement("save(object)")
                .endControlFlow()
                .addStatement("getDatabase().setTransactionSuccessful()")
                .nextControlFlow("finally")
                .addStatement("getDatabase().endTransaction()")
                .endControlFlow();

        methods.add(builder.build());

        builder = MethodSpec.methodBuilder("saveInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(typeName, "first", Modifier.FINAL)
                .addParameter(ArrayTypeName.of(typeName), "others", Modifier.FINAL)
                .varargs()
                .addStatement("getDatabase().beginTransaction()")
                .beginControlFlow("try")
                .addStatement("save(first)")
                .beginControlFlow("for ($T object : others)", typeName)
                .addStatement("save(object)")
                .endControlFlow()
                .addStatement("getDatabase().setTransactionSuccessful()")
                .nextControlFlow("finally")
                .addStatement("getDatabase().endTransaction()")
                .endControlFlow();

        methods.add(builder.build());

        // Insert
        builder = MethodSpec.methodBuilder("insert")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(typeName, entityNameLC)
                .addStatement("$T statement = getInsertStatement()", JoogarClasses.androidSQLiteStatement)
                .addStatement("bindInsertStatement($L, statement)", entityNameLC)
                .addStatement("long id = statement.executeInsert()");

        if (getPrimaryKeys().size() == 1) {
            Column id = getPrimaryKeys().get(0);
            if (id.isIntegerType()) {
                String idName;
                if (id.getTypeName().equals(TypeName.INT) || id.getTypeName().equals(TypeName.INT.box())) {
                    idName = "(int) id";
                } else {
                    idName = "id";
                }
                builder.addStatement("$L." + id.getMutator(), entityNameLC, idName);
            }
        }

        builder.addStatement("return id");

        methods.add(builder.build());

        // Insert in transaction
        builder = MethodSpec.methodBuilder("insertInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(ParameterizedTypeName.get(ClassName.get(Collection.class), typeName), "objects", Modifier.FINAL)
                .addStatement("getDatabase().beginTransaction()")
                .beginControlFlow("try")
                .beginControlFlow("for ($T object : objects)", typeName)
                .addStatement("insert(object)")
                .endControlFlow()
                .addStatement("getDatabase().setTransactionSuccessful()")
                .nextControlFlow("finally")
                .addStatement("getDatabase().endTransaction()")
                .endControlFlow();

        methods.add(builder.build());

        builder = MethodSpec.methodBuilder("insertInTx")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(typeName, "first", Modifier.FINAL)
                .addParameter(ArrayTypeName.of(typeName), "others", Modifier.FINAL)
                .varargs()
                .addStatement("getDatabase().beginTransaction()")
                .beginControlFlow("try")
                .addStatement("insert(first)")
                .beginControlFlow("for ($T object : others)", typeName)
                .addStatement("insert(object)")
                .endControlFlow()
                .addStatement("getDatabase().setTransactionSuccessful()")
                .nextControlFlow("finally")
                .addStatement("getDatabase().endTransaction()")
                .endControlFlow();

        methods.add(builder.build());

        // Insert with ContentValues
        builder = MethodSpec.methodBuilder("insert")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(JoogarClasses.androidContentValues, "values")
                .addStatement("return $L.insert($L, $L, $L)",
                        "getDatabase()", "getName()", "null", "values");
        methods.add(builder.build());

        builder = MethodSpec.methodBuilder("insertWithOnConflict")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(JoogarClasses.androidContentValues, "values")
                .addParameter(int.class, "conflictAlgorithm")
                .addStatement("return getDatabase().insertWithOnConflict(getName(), null, values, conflictAlgorithm)",
                        "getDatabase()", "getName()", "null", "values", "conflictAlgorithm");
        methods.add(builder.build());

        // Update
        // Update methods are meaningless if all columns are primary keys
        if (getColumns().size() > getPrimaryKeys().size()) {
            builder = MethodSpec.methodBuilder("update")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(boolean.class)
                    .addParameter(typeName, entityNameLC)
                    .addStatement("$T statement = getUpdateStatement()", JoogarClasses.androidSQLiteStatement)
                    .addStatement("bindUpdateStatement($L, statement)", entityNameLC)
                    .addStatement("return statement.executeUpdateDelete() == 1");

            methods.add(builder.build());

            //update in transaction
            builder = MethodSpec.methodBuilder("updateInTx")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .addParameter(ParameterizedTypeName.get(ClassName.get(Collection.class), typeName), "objects", Modifier.FINAL)
                    .returns(int.class)
                    .addStatement("int count = 0")
                    .addStatement("$L.beginTransaction()", "getDatabase()")
                    .beginControlFlow("try")
                    .beginControlFlow("for ($T object : objects)", typeName)
                    .addStatement("count += update(object) ? 1 : 0")
                    .endControlFlow()
                    .addStatement("$L.setTransactionSuccessful()", "getDatabase()")
                    .nextControlFlow("finally")
                    .addStatement("$L.endTransaction()", "getDatabase()")
                    .endControlFlow()
                    .addStatement("return count");


            methods.add(builder.build());

            builder = MethodSpec.methodBuilder("updateInTx")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .addParameter(typeName, "first", Modifier.FINAL)
                    .addParameter(ArrayTypeName.of(typeName), "others", Modifier.FINAL)
                    .returns(int.class)
                    .varargs()
                    .addStatement("int count = 0")
                    .addStatement("$L.beginTransaction()", "getDatabase()")
                    .beginControlFlow("try")
                    .addStatement("count += update(first) ? 1 : 0")
                    .beginControlFlow("for ($T object : others)", typeName)
                    .addStatement("count += update(object) ? 1 : 0")
                    .endControlFlow()
                    .addStatement("$L.setTransactionSuccessful()", "getDatabase()")
                    .nextControlFlow("finally")
                    .addStatement("$L.endTransaction()", "getDatabase()")
                    .endControlFlow()
                    .addStatement("return count");

            methods.add(builder.build());

            // Update all
            builder = MethodSpec.methodBuilder("update")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(int.class)
                    .addParameter(JoogarClasses.androidContentValues, "values")
                    .addStatement("return $L.update($L, $L, null, null)", "getDatabase()", "getName()", "values");

            methods.add(builder.build());

            // Update with where
            builder = MethodSpec.methodBuilder("update")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(int.class)
                    .addParameter(JoogarClasses.androidContentValues, "values")
                    .addParameter(JoogarClasses.entityWhere(entityName), "where")
                    .addStatement("return $L.update($L, $L, $L.$L, $L.$L)", "getDatabase()", "getName()",
                            "values", "where", "getWhere()", "where", "getArgs()");

            methods.add(builder.build());

            // Update with where & onConflict
            builder = MethodSpec.methodBuilder("updateWithOnConflict")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(int.class)
                    .addParameter(JoogarClasses.androidContentValues, "values")
                    .addParameter(JoogarClasses.entityWhere(entityName), "where")
                    .addParameter(int.class, "conflictAlgorithm")
                    .addStatement("return $L.updateWithOnConflict($L, $L, $L.$L, $L.$L, $L)", "getDatabase()", "getName()",
                            "values", "where", "getWhere()", "where", "getArgs()", "conflictAlgorithm");

            methods.add(builder.build());
        }

        return methods;
    }

    //Jan Bína
    public List<MethodSpec> createJoogarRecordMethods() {
        List<MethodSpec> result = new ArrayList<>();
        MethodSpec.Builder builder;
        String entityNameLC = Utils.lowerCaseFirst(entityName);

        //Save
        builder = MethodSpec.methodBuilder("save")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(typeName, entityNameLC)
                .addStatement("return $T.save($L)", helperClass, entityNameLC);
        result.add(builder.build());

        //Insert
        builder = MethodSpec.methodBuilder("insert")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(long.class)
                .addParameter(typeName, entityNameLC)
                .addStatement("return $T.insert($L)", helperClass, entityNameLC);
        result.add(builder.build());

        //Update
        // Update methods are meaningless if all columns are primary keys
        if (getColumns().size() > getPrimaryKeys().size()) {
            builder = MethodSpec.methodBuilder("update")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(boolean.class)
                    .addParameter(typeName, entityNameLC)
                    .addStatement("return $T.update($L)", helperClass, entityNameLC);
            result.add(builder.build());
        }

        //Delete
        builder = MethodSpec.methodBuilder("delete")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(typeName, entityNameLC)
                .returns(boolean.class)
                .addStatement("return $T.delete($L)", helperClass, entityNameLC);
        result.add(builder.build());

        return result;
    }

    //Jan Bína
    public TypeSpec createWhereStatementClass() {
        ClassName whereTypeName = JoogarClasses.entityWhere(entityName);

        TypeSpec.Builder builder = TypeSpec.classBuilder(whereTypeName)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .superclass(JoogarClasses.whereStatement(whereTypeName));

        for (Column column : getColumns()) {
            TypeName columnType = column.getTypeName();
            if (columnType.isPrimitive()) {
                columnType = columnType.box();
            }
            builder.addMethod(
                    MethodSpec.methodBuilder(column.getVariableName())
                            .addModifiers(Modifier.PUBLIC)
                            .returns(JoogarClasses.comparator(whereTypeName, columnType))
                            .addStatement("appendColumn($S)", column.getName())
                            .addStatement("return new $T(this)", JoogarClasses.comparator(whereTypeName, columnType))
                            .build()
            );
        }

        return builder.build();
    }

    //Pavel Salva
    public CodeBlock createTableBlock(String database, String entity) {
        CodeBlock.Builder builder = CodeBlock.builder();

        String indexListName = null;
        if (tableIndex != null || tableIndexes != null) {
            indexListName = getName() + "Index";
            builder.add("$T<$T> $L = new $T<>();\n", List.class, JoogarClasses.tableIndex, indexListName, ArrayList.class);
            if (tableIndex != null) {
                builder.add("$L.add(new $T(new $T[] $L, $L));\n", indexListName, JoogarClasses.tableIndex, String.class, Utils.arrayToString(tableIndex.columns()), tableIndex.unique());
            }
            if (tableIndexes != null && tableIndexes.indexes() != null && tableIndexes.indexes().length > 0) {
                for (TableIndex ti : tableIndexes.indexes()) {
                    builder.add("$L.add(new $T(new $T[] $L, $L));\n", indexListName, JoogarClasses.tableIndex, String.class, Utils.arrayToString(ti.columns()), ti.unique());
                }
            }
        }

        builder.add("$L = new $T($S, $S, $L, $L);\n",
                entity, JoogarClasses.tableJoogar, getName(), getEntityName(), indexListName,
                getPrimaryKeys().size());

        for (Column column : getColumns()) {
            builder.add(column.createColumnBlock(entity));
        }

        builder.add("$L.addTable($L);\n", database, entity);

        return builder.build();
    }


}
