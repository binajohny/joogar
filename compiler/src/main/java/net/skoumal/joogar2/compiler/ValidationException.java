package net.skoumal.joogar2.compiler;

import javax.lang.model.element.Element;

/**
 * Created by Jan Bína
 */

public class ValidationException extends Exception {

    private Element element;

    public ValidationException(String s, Element element) {
        super(s);
        this.element = element;
    }

    public Element getElement() {
        return element;
    }
}
