package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.CodeBlock;

/**
 * Created by Jan Bína
 */

public class TransactionCodeBlock {
    private CodeBlock.Builder codeblock;

    private TransactionCodeBlock(){}

    public static TransactionCodeBlock builder() {
        TransactionCodeBlock transactionCodeBlock = new TransactionCodeBlock();
        transactionCodeBlock.init();
        return transactionCodeBlock;
    }

    private void init() {
        codeblock = CodeBlock.builder()
                .beginControlFlow("new $T()", JoogarClasses.transaction)
                .add("@Override\n")
                .beginControlFlow("public void execute()");
    }

    public CodeBlock build() {
        return codeblock
                .endControlFlow()
                .unindent()
                .add("}")
                .build();
    }

    public TransactionCodeBlock addStatement(String format, Object... args) {
        codeblock.addStatement(format, args);
        return this;
    }

    public TransactionCodeBlock add(String format, Object... args) {
        codeblock.add(format, args);
        return this;
    }

    public TransactionCodeBlock beginControlFlow(String format, Object... args) {
        codeblock.beginControlFlow(format, args);
        return this;
    }

    public TransactionCodeBlock nextControlFlow(String format, Object... args) {
        codeblock.nextControlFlow(format, args);
        return this;
    }

    public TransactionCodeBlock endControlFlow(String format, Object... args) {
        codeblock.endControlFlow(format, args);
        return this;
    }

    public TransactionCodeBlock endControlFlow() {
        codeblock.endControlFlow();
        return this;
    }

}
