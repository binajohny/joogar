package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

/**
 * Created by Jan Bína
 */

public final class JoogarClasses {

    private JoogarClasses() {}

    public static final String BASE_PKG = "net.skoumal.joogar2";

    public static final ClassName joogarBase =
            ClassName.get(BASE_PKG + ".shared", "JoogarBase");

    public static final ClassName joogar =
            ClassName.get(BASE_PKG + ".shared", "Joogar");

    public static final ClassName entityBuilder =
            ClassName.get(BASE_PKG, "EntityBuilder");

    public static final ClassName joogarDatabaseResult =
            ClassName.get(BASE_PKG + ".shared", "JoogarDatabaseResult");

    public static final ClassName findStatement =
            ClassName.get(BASE_PKG, "FindStatement");

    public static final ClassName whereStatement =
            ClassName.get(BASE_PKG, "WhereStatement");

    public static final ClassName comparator =
            ClassName.get(BASE_PKG + ".WhereStatement", "Comparator");

    public static final ClassName columnJoogar =
            ClassName.get(BASE_PKG + ".shared.model", "ColumnJoogar");

    public static final ClassName tableJoogar =
            ClassName.get(BASE_PKG + ".shared.model", "TableJoogar");

    public static final ClassName databaseJoogar =
            ClassName.get(BASE_PKG + ".shared.model", "DatabaseJoogar");

    public static final ClassName joogarDatabaseBuilder =
            ClassName.get(BASE_PKG + ".shared", "JoogarDatabaseBuilder");

    public static final ClassName columnProperty =
            ClassName.get(BASE_PKG, "ColumnProperty");

    public static final ClassName androidContext =
            ClassName.get("android.content", "Context");

    public static final ClassName androidContentValues =
            ClassName.get("android.content", "ContentValues");

    public static final ClassName androidSQLiteStatement =
            ClassName.get("android.database.sqlite", "SQLiteStatement");

    public static final ClassName joogarCursor =
            ClassName.get(BASE_PKG + ".shared.cursor", "JoogarCursor");

    public static final ClassName  tableIndex =
            ClassName.get(BASE_PKG + ".shared.model", "TableIndex");

    public static final ClassName transaction =
            ClassName.get(BASE_PKG, "Transaction");

    public static final ClassName utils =
            ClassName.get(BASE_PKG, "Utils");

    public static final ClassName joogarDatabase =
            ClassName.get(BASE_PKG + ".shared", "JoogarDatabase");

    public static TypeName entityBuilder(TypeName inner) {
        return ParameterizedTypeName.get(entityBuilder, inner);
    }

    public static TypeName findStatement(TypeName inner1, TypeName inner2) {
        return ParameterizedTypeName.get(findStatement, inner1, inner2);
    }

    public static TypeName comparator(TypeName inner1, TypeName inner2) {
        return ParameterizedTypeName.get(comparator, inner1, inner2);
    }

    public static ClassName entityWhere(String entityName) {
        return ClassName.get(BASE_PKG, Utils.upperCaseFirst(entityName)+"Where");
    }

    public static TypeName columnProperty(TypeName inner) {
        return ParameterizedTypeName.get(columnProperty, inner);
    }

    public static TypeName joogarCursor(TypeName inner) {
        return ParameterizedTypeName.get(joogarCursor, inner);
    }

    public static TypeName whereStatement(TypeName inner) {
        return ParameterizedTypeName.get(whereStatement, inner);
    }
}
