package net.skoumal.joogar2.compiler;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;

/**
 * Created by Jan Bína
 */

final class Utils {

    private Utils() {}

    public static boolean hasDefaultNonPrivateConstructor(TypeElement type) {
        for (ExecutableElement ctor : ElementFilter.constructorsIn(type.getEnclosedElements())) {
            if (ctor.getParameters().isEmpty() && !ctor.getModifiers().contains(Modifier.PRIVATE)) {
                return true;
            }
        }

        return false;
    }

    public static String getSQLiteHelperName(String dbName) {
        return dbName.substring(0,1).toUpperCase() + dbName.substring(1) + "SQLiteHelper";
    }

    public static String lowerCaseFirst(String str) {
        return str.substring(0,1).toLowerCase() + str.substring(1);
    }

    public static String upperCaseFirst(String str) {
        return str.substring(0,1).toUpperCase() + str.substring(1);
    }

    public static String getFindMethodName(String entityName) {
        return "find" + Utils.upperCaseFirst(entityName);
    }

    public static String arrayToString(String[] array) {
        if (array == null || array.length == 0) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("{");

        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append("\"");
            sb.append(array[i]);
            sb.append("\"");
        }

        sb.append("}");
        return sb.toString();
    }

    public static String md5(String text) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(text.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

}

