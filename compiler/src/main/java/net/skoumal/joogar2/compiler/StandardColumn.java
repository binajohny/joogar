package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;

/**
 * Created by Jan Bína
 */

public class StandardColumn extends Column {

    public StandardColumn(String name, TypeName typeName, VariableElement variable,
                          ExecutableElement getter, ExecutableElement setter,
                          boolean primaryKey, boolean unique, boolean nonNull, boolean index) {
        super(name, typeName, variable, getter, setter, primaryKey, unique, nonNull, index);
    }

    @Override
    public CodeBlock fromDbResult(String entityName, String resultName, int columnIndex) {
        CodeBlock.Builder builder = CodeBlock.builder();
        if (!getTypeName().isPrimitive()) {
            builder.beginControlFlow("if (!$L.isNull($L))", resultName, columnIndex);
        }

        builder.addStatement("$L." + getMutator(), entityName, getDatabaseResultMethod(resultName, columnIndex));

        if (!getTypeName().isPrimitive()) {
            builder.endControlFlow();
        }
        return builder.build();
    }

    @Override
    public CodeBlock bindStatement(String accessor, String statement, int index, boolean excludeIds) {
        CodeBlock.Builder builder = CodeBlock.builder();

        String ifStmt = "";

        if (!getTypeName().isPrimitive()) {
            ifStmt = accessor + " != null";
        }

        if (excludeIds && isPrimaryKey() && isIntegerType()) {
            if (ifStmt.length() > 0) {
                ifStmt += " && ";
            }
            ifStmt += accessor + " > 0";
        }

        if (ifStmt.length() > 0) {
            builder.beginControlFlow("if ($L)", ifStmt);
        }

        builder.add(getBinderMethodName(accessor, statement, index)).add(";\n");

        if (ifStmt.length() > 0) {
            builder.endControlFlow();
        }

        return builder.build();
    }

    @Override
    public CodeBlock createColumnBlock(String entity) {
        CodeBlock.Builder builder = CodeBlock.builder();
        builder.add("$L.addColumn(new $T($S, $L, $L, $L, $S, $L));\n", entity, JoogarClasses.columnJoogar,
                getName(), isPrimaryKey(), isUnique(), isNonNull(), getSqliteType(), isIndex());
        return builder.build();
    }

    @Override
    public FieldSpec createTableHelperProperty(TypeName typeName) {
        FieldSpec.Builder builder = FieldSpec.builder(typeName, getVariableName(),
                Modifier.PUBLIC, Modifier.STATIC)
                .initializer("new $T($S)", typeName, getName());
        return builder.build();
    }

    @Override
    public String getAccessor() {
        if (getGetter() != null) {
            return getGetter().getSimpleName().toString() + "()";
        } else {
            return getVariable().getSimpleName().toString();
        }
    }

    @Override
    public String getMutator() {
        if (getSetter() != null) {
            return getSetter().getSimpleName().toString() + "($L)";
        } else {
            return getVariable().getSimpleName().toString() + " = $L";
        }
    }
}
