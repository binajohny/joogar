package net.skoumal.joogar2.compiler;

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import net.skoumal.joogar2.annotations.ColumnName;
import net.skoumal.joogar2.annotations.Ignore;
import net.skoumal.joogar2.annotations.Index;
import net.skoumal.joogar2.annotations.NonNull;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;
import net.skoumal.joogar2.annotations.TableIndex;
import net.skoumal.joogar2.annotations.TableIndexes;
import net.skoumal.joogar2.annotations.Unique;

import java.beans.Introspector;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;


/**
 * Created by Jan Bína & Pavel Salva
 */

@AutoService(Processor.class)
public class JoogarProcessor extends AbstractProcessor {

    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        messager = processingEnv.getMessager();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(Table.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    //Jan Bína
    private void validateTableClass(TypeElement tableAnnotated) throws ValidationException {
        Table table = tableAnnotated.getAnnotation(Table.class);
        if (table == null) {
            // This shouldn't happen unless the compilation environment is buggy,
            // but it has happened in the past and can crash the compiler.
            throw new ValidationException("Annotation processor for @Table was invoked with an entity"
                    + " that does not have that annotation; this is probably a compiler bug", tableAnnotated);
        }
        if (tableAnnotated.getKind() != ElementKind.CLASS) {
            throw new ValidationException("@Table annotation only applies to classes", tableAnnotated);
        }

        // Class must be public
        if (!tableAnnotated.getModifiers().contains(Modifier.PUBLIC)) {
            throw new ValidationException("Class annotated with @Table must be public.", tableAnnotated);
        }

        // Class must not be abstract
        if (tableAnnotated.getModifiers().contains(Modifier.ABSTRACT)) {
            throw new ValidationException("Class annotated with @Table must not be abstract.", tableAnnotated);
        }

        // Class must have default non private constructor
        if (!Utils.hasDefaultNonPrivateConstructor(tableAnnotated)) {
            throw new ValidationException("Class annotated with @Table must have default non private constructor.", tableAnnotated);
        }
    }

    //Jan Bína
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        Map<String, Database> databases = new HashMap<>();

        for (TypeElement tableAnnotated : ElementFilter.typesIn(roundEnv.getElementsAnnotatedWith(Table.class))) {

            try {
                validateTableClass(tableAnnotated);
            } catch (ValidationException e) {
                messager.printMessage(Diagnostic.Kind.ERROR, e.getMessage(), e.getElement());
            }


            //Now we have valid Table class
            Table table = tableAnnotated.getAnnotation(Table.class);
            TableIndex tableIndex = tableAnnotated.getAnnotation(TableIndex.class);
            TableIndexes tableIndexes = tableAnnotated.getAnnotation(TableIndexes.class);
            String databaseName = table.database();

            if (databaseName.isEmpty()) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Database name cannot be empty.", tableAnnotated);
            }

            Database database = databases.get(databaseName);
            if (database == null) {
                database = new Database(databaseName);
                databases.put(databaseName, database);
            }


            String tableName = table.name();
            if (tableName.isEmpty()) {
                tableName = Introspector.decapitalize(tableAnnotated.getSimpleName().toString());
            }

            TableEntity tableEntity = new TableEntity(tableName, tableAnnotated.getSimpleName().toString(), TypeName.get(tableAnnotated.asType()), tableAnnotated, tableIndex, tableIndexes);

            List<Column> columns = getColumnsForTable(tableAnnotated);

            for (Column column : columns) {
                boolean success = tableEntity.addColumn(column.getName(), column);
                if (!success) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "Entity has more columns named " + column.getName(), tableAnnotated);
                }
            }

            if (tableEntity.getPrimaryKeys().isEmpty()) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Entity don't have primary key", tableAnnotated);
            }

            boolean success = database.addTable(tableName, tableEntity);
            if (!success) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Database " + databaseName + " has more tables named " + tableName, tableAnnotated);
            }
        }

        process(databases.values());

        return false;
    }

    //Jan Bína
    private void process(Collection<Database> databases) {
        List<MethodSpec> joogarMethods = new ArrayList<>();
        List<MethodSpec> joogarRecordMethods = new ArrayList<>();
        List<TypeSpec> newClasses = new ArrayList<>();
        List<String> databaseCreationMethods = new ArrayList<>();
        List<JavaFile> tableHelperFiles = new ArrayList<>();

        for (Database database : databases) {
            joogarRecordMethods.addAll(database.createJoogarRecordMethods());
            newClasses.addAll(database.createWhereStatementClasses());
            tableHelperFiles.addAll(database.createTableHelperFiles());

            MethodSpec createDatabaseMethod = database.createDatabaseMethod();
            databaseCreationMethods.add(createDatabaseMethod.name);

            joogarMethods.add(createDatabaseMethod);
            joogarMethods.add(database.createDatabaseCustomMigrationMethod());
        }


        createTableHelperFiles(tableHelperFiles);
        createJoogarClass(joogarMethods, databaseCreationMethods, databases);
        createJoogarRecordClass(joogarRecordMethods);
        createNewClasses(newClasses);
    }

    //Jan Bína
    private void createTableHelperFiles(List<JavaFile> files) {
        for (JavaFile file : files) {
            createFile(file);
        }
    }

    //Jan Bína & Pavel Salva
    private void createJoogarClass(List<MethodSpec> joogarMethods, List<String> databaseCreationMethods, Collection<Database> databases) {
        if (joogarMethods.isEmpty()) {
            return;
        }

        MethodSpec.Builder init = MethodSpec.methodBuilder("init")
                .addModifiers(Modifier.PUBLIC).addModifiers(Modifier.STATIC)
                .addParameter(JoogarClasses.androidContext, "context")
                .beginControlFlow("if (instance != null)")
                .addStatement("throw new $T($S)", IllegalStateException.class, "Joogar already initialized.")
                .endControlFlow()
                .addStatement("instance = new $T()", JoogarClasses.joogar)
                .addStatement("instance.baseInit($L)", "context")
                .addStatement("instance.createDatabases()");

        MethodSpec.Builder createDatabases = MethodSpec.methodBuilder("createDatabases")
                .addModifiers(Modifier.PROTECTED, Modifier.FINAL)
                .addAnnotation(Override.class)
                .addStatement("super.createDatabases()");

        for (String methodName : databaseCreationMethods) {
            createDatabases.addStatement("$L()", methodName);
        }

        MethodSpec.Builder getInstance = MethodSpec.methodBuilder("getInstance")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(JoogarClasses.joogar)
                .addStatement("return instance");

        MethodSpec.Builder close = MethodSpec.methodBuilder("close")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class);

        for (Database database : databases) {
            for (TableEntity tableEntity : database.getTables()) {
                close.addStatement("$T.close()", tableEntity.getHelperClass());
            }
        }

        close.addStatement("instance = null")
                .addStatement("super.close()");

        TypeSpec joogar = TypeSpec.classBuilder(JoogarClasses.joogar.simpleName())
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addField(JoogarClasses.joogar, "instance", Modifier.PRIVATE, Modifier.STATIC)
                .superclass(JoogarClasses.joogarBase)
                .addMethods(joogarMethods)
                .addMethod(init.build())
                .addMethod(createDatabases.build())
                .addMethod(getInstance.build())
                .addMethod(close.build())
                .build();

        createFile(JoogarClasses.joogar.packageName(), joogar);
    }

    //Jan Bína
    private void createJoogarRecordClass(List<MethodSpec> methods) {
        if (methods.isEmpty()) {
            return;
        }

        TypeSpec joogarRecord = TypeSpec.classBuilder("JoogarRecord")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addMethods(methods)
                .build();

        createFile(JoogarClasses.BASE_PKG, joogarRecord);
    }

    //Jan Bína
    private void createNewClasses(List<TypeSpec> classes) {
        for (TypeSpec clazz : classes) {
            createFile(JoogarClasses.BASE_PKG, clazz);
        }
    }

    //Jan Bína
    private void createFile(String pkg, TypeSpec clazz) {
        JavaFile file = JavaFile.builder(
                pkg,
                clazz
        ).skipJavaLangImports(true).indent("    ").build();

        createFile(file);
    }

    //Jan Bína
    private void createFile(JavaFile file) {
        try {
            file.writeTo(processingEnv.getFiler());
        } catch (IOException e) {
            e.printStackTrace();
            messager.printMessage(Diagnostic.Kind.ERROR, e.toString());
        }
    }

    //Jan Bína
    private List<Column> getColumnsForTable(TypeElement tableAnnotated) {
        List<VariableElement> variables = fieldsIn(tableAnnotated);
        List<ExecutableElement> methods = methodsIn(tableAnnotated);
        ImmutableMap<String, ExecutableElement> getters = gettersIn(methods);
        ImmutableMap<String, ExecutableElement> setters = settersIn(methods);

        List<Column> columns = new ArrayList<>();

        for (VariableElement var : variables) {

            if (var.getAnnotation(Ignore.class) != null) {
                continue;
            }

            // Ignore static fields by default
            if (var.getModifiers().contains(Modifier.STATIC)) {
                continue;
            }

            ExecutableElement getter = getters.get(var.getSimpleName().toString());
            if (getter == null ||
                    getter.getModifiers().contains(Modifier.PRIVATE) ||
                    getter.getModifiers().contains(Modifier.STATIC) ||
                    !getter.getParameters().isEmpty() ||
                    !getter.getReturnType().equals(var.asType())) {
                getter = null;
            }

            ExecutableElement setter = setters.get(var.getSimpleName().toString());
            if (setter == null ||
                    setter.getModifiers().contains(Modifier.PRIVATE) ||
                    setter.getModifiers().contains(Modifier.STATIC) ||
                    setter.getParameters().size() != 1 ||
                    !setter.getParameters().get(0).asType().equals(var.asType()) ||
                    !setter.getReturnType().getKind().equals(TypeKind.VOID)) {
                setter = null;
            }

            if (var.getModifiers().contains(Modifier.PRIVATE) &&
                    (getter == null || setter == null)) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Property must not be private or have non private getter and setter", var);
            }

            String columnName;
            ColumnName nameAnnotation = var.getAnnotation(ColumnName.class);
            if (nameAnnotation != null) {
                if (nameAnnotation.value().isEmpty()) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "Column name cannot be empty", var);
                }
                columnName = nameAnnotation.value();
            } else {
                columnName = var.getSimpleName().toString();
            }

            TypeName typeName = TypeName.get(var.asType());

            if (!isValidType(typeName)) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Property is of unsupported type " + typeName, var);
            } else {
                columns.add(
                        new StandardColumn(columnName, typeName, var, getter, setter,
                                var.getAnnotation(PrimaryKey.class) != null,
                                var.getAnnotation(Unique.class) != null,
                                var.getAnnotation(NonNull.class) != null,
                                var.getAnnotation(Index.class) != null)
                );
            }
        }

        return columns;
    }

    //Jan Bína
    private List<VariableElement> fieldsIn(TypeElement element) {
        List<VariableElement> result = new ArrayList<>();

        result.addAll(ElementFilter.fieldsIn(element.getEnclosedElements()));

        TypeMirror superType;
        while (!((superType = element.getSuperclass()) instanceof NoType)) {
            element = (TypeElement) ((DeclaredType) superType).asElement();
            result.addAll(ElementFilter.fieldsIn(element.getEnclosedElements()));
        }

        return result;
    }

    //Jan Bína
    private List<ExecutableElement> methodsIn(TypeElement element) {
        List<ExecutableElement> result = new ArrayList<>();

        result.addAll(ElementFilter.methodsIn(element.getEnclosedElements()));

        TypeMirror superType;
        while (!((superType = element.getSuperclass()) instanceof NoType)) {
            element = (TypeElement) ((DeclaredType) superType).asElement();
            result.addAll(ElementFilter.methodsIn(element.getEnclosedElements()));
        }

        return result;
    }

    //Jan Bína
    private static boolean isValidType(TypeName typeName) {
        return typeName.isPrimitive() ||
                typeName.isBoxedPrimitive() ||
                typeName.equals(TypeName.get(String.class)) ||
                typeName.equals(TypeName.get(byte[].class)) ||
                typeName.equals(TypeName.get(Byte[].class));
    }

    //Jan Bína
    private static ImmutableMap<String, ExecutableElement> gettersIn(Iterable<ExecutableElement> methods) {
        ImmutableMap.Builder<String, ExecutableElement> getters = ImmutableMap.builder();
        for (ExecutableElement method : methods) {
            String name = method.getSimpleName().toString();
            if ((name.startsWith("get") && !name.equals("get")) ||
                    (name.startsWith("is") && !name.equals("is")
                            && method.getReturnType().getKind() == TypeKind.BOOLEAN)) {
                getters.put(nameWithoutPrefix(name), method);
            }
        }
        return getters.build();
    }

    //Jan Bína
    private static ImmutableMap<String, ExecutableElement> settersIn(Iterable<ExecutableElement> methods) {
        ImmutableMap.Builder<String, ExecutableElement> setters = ImmutableMap.builder();
        for (ExecutableElement method : methods) {
            String name = method.getSimpleName().toString();
            if (name.startsWith("set") && !name.equals("set")) {
                setters.put(nameWithoutPrefix(name), method);
            }
        }
        return setters.build();
    }

    //Jan Bína
    private static String nameWithoutPrefix(String name) {
        if (name.startsWith("get") || name.startsWith("set")) {
            name = name.substring(3);
        } else if (name.startsWith("is")) {
            name = name.substring(2);
        }

        return Introspector.decapitalize(name);
    }
}

