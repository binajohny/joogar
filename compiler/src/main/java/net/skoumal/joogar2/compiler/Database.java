package net.skoumal.joogar2.compiler;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeSpec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Modifier;

/**
 * Created by Jan Bína & Pavel Salva
 */

public class Database {
    private String name;
    private Map<String, TableEntity> tables;

    public Database(String name) {
        this.name = name;
        this.tables = new HashMap<>();
    }

    //Jan Bína
    public boolean addTable(String name, TableEntity table) {
        if (tables.get(name) != null) {
            return false;
        }
        tables.put(name, table);
        return true;
    }

    public Collection<TableEntity> getTables() {
        return tables.values();
    }

    //Pavel Salva
    public MethodSpec createDatabaseMethod() {

        StringBuilder textForHash = new StringBuilder();

        MethodSpec.Builder databaseCreationMethod = MethodSpec.methodBuilder("create" + Utils.upperCaseFirst(name) + "Database")
                .addModifiers(Modifier.PRIVATE, Modifier.FINAL);

        databaseCreationMethod.addStatement("$1T database = new $1T($2S)", JoogarClasses.databaseJoogar, name);

        databaseCreationMethod.addStatement("$T entity", JoogarClasses.tableJoogar);
        for (TableEntity table : getTables()) {
            CodeBlock codeBlock =table.createTableBlock("database", "entity");
            textForHash.append(codeBlock.toString());
            databaseCreationMethod.addCode(codeBlock);
        }

        String md5 = Utils.md5(textForHash.toString());
        databaseCreationMethod.addStatement("database.addHash($S)", md5);
        databaseCreationMethod.addStatement("$1T builder = new $1T().setDatabaseSchema(database)", JoogarClasses.joogarDatabaseBuilder);
        databaseCreationMethod.addStatement("instance.addDB(builder)");

        return databaseCreationMethod.build();
    }

    //Pavel Salva
    public MethodSpec createDatabaseCustomMigrationMethod() {

        ParameterSpec migrationsArray = ParameterSpec.builder(String[].class, "migrationsArray")
                .addModifiers()
                .build();

        MethodSpec.Builder databaseCustomMigrationMethod = MethodSpec.methodBuilder("set" + Utils.upperCaseFirst(name) + "DatabaseCustomMigrations")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addParameter(migrationsArray)
                .addStatement("setDatabaseCustomMigrations(migrationsArray, $S)", name);

        return databaseCustomMigrationMethod.build();
    }

    //Jan Bína
    public List<JavaFile> createTableHelperFiles() {
        List<JavaFile> files = new ArrayList<>();
        for (TableEntity table : getTables()) {
            files.add(table.createTableHelperFile(name));
        }
        return files;
    }

    //Jan Bína
    public List<MethodSpec> createJoogarRecordMethods() {
        List<MethodSpec> result = new ArrayList<>();

        for (TableEntity table : getTables()) {
            result.addAll(table.createJoogarRecordMethods());
        }

        return result;
    }

    //Jan Bína
    public List<TypeSpec> createWhereStatementClasses() {
        List<TypeSpec> result = new ArrayList<>();

        for (TableEntity table : getTables()) {
            result.add(table.createWhereStatementClass());
        }

        return result;
    }
}
