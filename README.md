# Joogar

Smart Android ORM developed by SKOUMAL studio.

<!---
## Gradle Dependency

Add this to your module's `build.gradle` file:

```gradle
dependencies {
    // ...
    compile 'net.skoumal.joogar2:joogar:0.1.9'
    provided 'net.skoumal.joogar2:joogar-annotations:0.1.9'
    annotationProcessor 'net.skoumal.joogar2:joogar-compiler:0.1.9'
}
```
-->

## Model classes

Class must be annotated with the `@Table` annotation. It has two optional parameters. Parameter `name` specifies table name, if not defined,
class name will be used. Parameter `database` specifies database name, if not defined, default database named `joogar` will be used.

To specify primary key of your table, use `@PrimaryKey` annotation.

You can use `@ColumnName` annotation to specify column name, `@Unique` annotation to set `UNIQUE` constraint on column,
`@NonNull` annotation to se `NOT NULL` constraint on column or `@Ignore` annotation for fields which shouldn't be stored in database.

Field must have at least package-private accessibility, but you can also use getters & setters.
No-args constructor is needed.

```java
@Table(name = "persons", database = "mydatabase")
public class Person {
    @PrimaryKey
    Long id;
    @ColumnName("first_name")
    String firstName;
    @NonNull
    String lastName;
    @Unique
    private String nickName;
    @Ignore
    int ignored;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
```

### Indexes
If you want single column index, you can just use `@Index` annotation.
If you want multiple column index, there is `@TableIndex` annotation.
Since Android doesn't support repeating annotations, if you want more than one
multiple column index, you must use `@TableIndexes` annotation, as you can see in the example below.

```java
@Table
@TableIndex(columns = {"firstName", "lastName"})
public class Person {
    @PrimaryKey
    protected Long id;
    protected String firstName;
    @Index
    protected String lastName;
}
```

```java
@Table
@TableIndexes (indexes = {
    @TableIndex (columns = {"firstName", "lastName"}),
    @TableIndex (columns = {"firstName", "nickName"})
})
public class Person {
    @PrimaryKey
    protected Long id;
    protected String firstName;
    protected String lastName;
    protected String nickName;
}
```

## Initialization

All you need to initialize joogar is calling `init` method, which needs application Context.
You can do this in your Application onCreate method.

Note that Joogar class is generated, so you must first define your tables and Rebuild project, before you can use it.
```java
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Joogar.init(this);
    }
}
```

## Inserting, updating, deleting

To insert your entities to database, you can use `insert` or `save` methods.

`insert` method inserts new row to database, if it will violate some constraint, it will throw
`SQLiteConstraintException`.

`save` method inserts new row to database, or updates existing row (`INSERT OR REPLACE`).

```java
Person joe = new Person("Joe");
Person james = new Person("James");

long id1 = JoogarRecord.insert(joe);
long id2 = JoogarRecord.save(james);
```

If you want to update existing record, you can use `update` method.

```java
james.setName("John");

boolean updated = JoogarRecord.update(james);
```

If you want to delete existing record, you can do so using `delete` or `deleteByPrimaryKey` method.
If you use single primary key, second method will be names after it, for example `deleteById`.
Since we cannot tell the table just from primary key, the second method must be called on special `Table` class,
which is generated for each table, see the example.

```java
boolean deleted = JoogarRecord.delete(james);

boolean deleted = PersonTable.deleteById(1);
```

### Transactions

If you want to insert, update or delete more entities at once, you can do so using methods like `insertInTx`.
These methods automatically create transaction. All this methods must be called on generated `Table` class.

```java
List<Person> people;
...

PersonTable.insertInTx(people);
PersonTable.saveInTx(people);
int count = PersonTable.updateInTx(people);
int count = PersonTable.deleteInTx(people);
```

## Reading

You can read single entity by primary key or multiple entities with find statement. See the example below.

```java
Person person = PersonTable.findById(3);

JoogarCursor<Person> people = PersonTable.find()
        .where(new PersonWhere().firstName().eq("Joe"))
        .orderBy(PersonTable.lastName)
        .limit(20)
        .find();
```

## Where

You can build where clause using `Where` class, which is generated for each table.
You can use `and` and `or` functions to concatenate conditions and even `and(Where)`, `or(Where)`
for nested conditions. Created where clause can be used in find statement, as you can see in the previous example,
as well as for deleting and updating.

```java
PersonWhere where = new PersonWhere()
        .firstName().in("Joe", "John", "Paul")
        .lastName().eq("Smith")
        .and(
                new PersonWhere()
                    .birthYear().gt(1990)
                    .or()
                    .birthYear().eq(1990).birthMonth().gtEq(10)
        );

```


# License

    Copyright 2015 SKOUMAL, s.r.o.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
