package net.skoumal.joogar2.example.model;

import net.skoumal.joogar2.annotations.Index;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */
@Table()
public class Note {
    @PrimaryKey
    public long id;
    public String title;
    public String note;
    @Index
    public int priority;
    @Index
    public String tag;
}
