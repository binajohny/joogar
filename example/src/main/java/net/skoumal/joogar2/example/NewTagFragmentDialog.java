package net.skoumal.joogar2.example;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.example.model.Tag;

/**
 * Created by Pavel Salva
 */

public class NewTagFragmentDialog extends AppCompatDialogFragment {

    private EditText etName;
    private Button btnSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_tag, container);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);

        getDialog().setTitle("New Tag");

        etName.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void initViews(View view) {
        etName = (EditText) view.findViewById(R.id.et_name);
        btnSave = (Button) view.findViewById(R.id.btn_save);

        prepareButton();

    }

    private void prepareButton() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTag();
            }
        });
    }

    private void saveTag() {
        Tag tag = new Tag();
        tag.name = etName.getText().toString();

        if(TextUtils.isEmpty(tag.name)){
            Toast.makeText(getContext(), "Tag cannot be empty!", Toast.LENGTH_LONG).show();
            return;
        }


        try {
            JoogarRecord.insert(tag);
        } catch (SQLiteConstraintException ex){
            Toast.makeText(getContext(), "Tag already exists!", Toast.LENGTH_LONG).show();
            return;
        }
        ((MainActivity)getActivity()).initTags();
        dismiss();
    }




}
