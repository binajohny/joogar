package net.skoumal.joogar2.example;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;

import net.skoumal.joogar2.ColumnProperty;
import net.skoumal.joogar2.FindStatement;
import net.skoumal.joogar2.NoteWhere;
import net.skoumal.joogar2.example.model.Note;
import net.skoumal.joogar2.example.model.NoteTable;
import net.skoumal.joogar2.example.model.Tag;
import net.skoumal.joogar2.example.model.TagTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Salva
 */


public class MainActivity extends AppCompatActivity {

    private ListView list;
    private NoteAdapter adapter;
    private FloatingActionButton newNoteButton;
    private TextView tvOrderPriority;
    private TextView tvOrderTag;
    private TextView tvOrderTitle;
    private ColumnProperty<Note> noteOrder;
    private LinearLayout llTags;
    private List<TextView> tvTagsList;
    private String tagFilter;
    private TextView tvAllTags;
    private Button btnAddTag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initViews();

        prepareListView();

        noteOrder = NoteTable.priority;
        tvOrderPriority.setBackgroundColor(Color.GRAY);
        tvAllTags.setBackgroundColor(Color.GRAY);
        getNotes();
    }

    private void prepareListView() {
        SwipeToDismissTouchListener<ListViewAdapter> touchListener = new SwipeToDismissTouchListener<>(
                new ListViewAdapter(list),
                new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                    @Override
                    public boolean canDismiss(int position) {
                        return (true);
                    }


                    @Override
                    public void onDismiss(ListViewAdapter view, int position) {
                        deleteNote(adapter.getNote(position));
                    }


                });


        list.setOnTouchListener(touchListener);
        list.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());

        if (touchListener.existPendingDismisses()) {
            touchListener.undoPendingDismiss();
        }
    }

    private void deleteNote(Note note) {
        NoteTable.delete(note);
        getNotes();
    }

    private void initViews() {

        list = (ListView) findViewById(R.id.lv_main);
        newNoteButton = (FloatingActionButton) findViewById(R.id.btn_new_note);
        tvOrderPriority = (TextView) findViewById(R.id.tv_order_priority);
        tvOrderTag = (TextView) findViewById(R.id.tv_order_tag);
        tvOrderTitle = (TextView) findViewById(R.id.tv_order_title);
        llTags = (LinearLayout) findViewById(R.id.ll_tags);
        tvAllTags = (TextView) findViewById(R.id.tv_tag_none);
        btnAddTag = (Button) findViewById(R.id.btn_new_tag);
        initOrderListeners();
        initTags();

        tvAllTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearTagsBackgroud();
                tagFilter = "";
                v.setBackgroundColor(Color.GRAY);
                getNotes();
            }
        });

        btnAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                new NewTagFragmentDialog().show(fm, "NewTagDialog");
            }
        });



    }

    public void initTags() {
        llTags.removeAllViews();
        tvTagsList = new ArrayList<>();
        tvTagsList.add(tvAllTags);

        List<Tag> tags = TagTable.find().orderBy(TagTable.name).find().toListAndClose();

        for (Tag tag : tags) {
            TextView tv = new TextView(this);
            tv.setText(tag.name);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tv.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            tv.setPadding(toPx(16,this), 0, 0, 0);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, toPx(40,this));
            llTags.addView(tv,lp);
            tvTagsList.add(tv);
            View view = new View(this);
            view.setBackgroundColor(Color.BLACK);
            LinearLayout.LayoutParams lpView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, toPx(1,this));
            llTags.addView(view, lpView);
            setTagListener(tv);
        }
    }

    private void setTagListener(final TextView tv) {
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearTagsBackgroud();
                tagFilter = tv.getText().toString();
                tv.setBackgroundColor(Color.GRAY);
                getNotes();
            }
        });
    }

    private void clearTagsBackgroud() {
        for(TextView tv : tvTagsList){
            tv.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public static int toPx(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int)px;
    }
    private void initOrderListeners() {
        newNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewNoteDialog();
            }
        });

        tvOrderPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderPriorityClick();
            }
        });

        tvOrderTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderTagClick();
            }

        });

        tvOrderTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderTitleClick();
            }
        });
    }

    private void orderTitleClick() {
        tvOrderTitle.setBackgroundColor(Color.GRAY);
        tvOrderPriority.setBackgroundColor(Color.TRANSPARENT);
        tvOrderTag.setBackgroundColor(Color.TRANSPARENT);
        noteOrder = NoteTable.title;
        getNotes();
    }

    private void orderTagClick() {
        tvOrderTag.setBackgroundColor(Color.GRAY);
        tvOrderPriority.setBackgroundColor(Color.TRANSPARENT);
        tvOrderTitle.setBackgroundColor(Color.TRANSPARENT);
        noteOrder = NoteTable.tag;
        getNotes();
    }

    private void orderPriorityClick() {
        tvOrderPriority.setBackgroundColor(Color.GRAY);
        tvOrderTitle.setBackgroundColor(Color.TRANSPARENT);
        tvOrderTag.setBackgroundColor(Color.TRANSPARENT);
        noteOrder = NoteTable.priority;
        getNotes();
    }

    private void openNewNoteDialog() {
        FragmentManager fm = getSupportFragmentManager();
        new NewNoteFragmentDialog().show(fm, "NewNoteDialog");
    }

    public void getNotes() {

        FindStatement<Note, NoteWhere> statement = NoteTable.find().orderBy(noteOrder);

        if (!TextUtils.isEmpty(tagFilter)) {
            NoteWhere where = new NoteWhere().tag().eq(tagFilter);
            statement = statement.where(where);
        }

        List<Note> notes = statement.find().toListAndClose();
        adapter = new NoteAdapter(this, notes);
        list.setAdapter(adapter);
    }


    public class NoteAdapter extends ArrayAdapter<Note> {

        private final List<Note> notesList;

        public NoteAdapter(Context context, List<Note> notes) {
            super(context, 0, notes);
            notesList = notes;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            Note note = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.note_item, parent, false);
            }

            TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            TextView tvNote = (TextView) convertView.findViewById(R.id.tv_note);
            TextView tvTag = (TextView) convertView.findViewById(R.id.tv_tag);
            TextView tvPriority = (TextView) convertView.findViewById(R.id.tv_priority);
            TextView tvDone = (TextView) convertView.findViewById(R.id.tv_done);

            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            tvTitle.setText(note.title);
            tvNote.setText(note.note);
            tvTag.setText(note.tag);
            tvPriority.setText("Priority: " + note.priority);


            return convertView;
        }

        public Note getNote(int position) {
            return notesList.get(position);
        }
    }

}
