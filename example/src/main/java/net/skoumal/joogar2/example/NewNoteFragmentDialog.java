package net.skoumal.joogar2.example;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.example.model.Note;
import net.skoumal.joogar2.example.model.Tag;
import net.skoumal.joogar2.example.model.TagTable;

import java.util.List;

/**
 * Created by Pavel Salva
 */

public class NewNoteFragmentDialog extends AppCompatDialogFragment {

    private EditText etTitle;
    private EditText etNote;
    private Spinner spinnerTag;
    private Spinner spinnerPriority;
    private Button btnSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_note, container);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);

        getDialog().setTitle("New Note");

        etTitle.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void initViews(View view) {
        etTitle = (EditText) view.findViewById(R.id.et_title);
        etNote = (EditText) view.findViewById(R.id.et_note);
        spinnerTag = (Spinner) view.findViewById(R.id.spinner_tag);
        spinnerPriority = (Spinner) view.findViewById(R.id.spinner_priority);
        btnSave = (Button) view.findViewById(R.id.btn_save);

        prepareTag();
        preparePriority();
        prepareButton();

    }

    private void prepareButton() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNote();
            }
        });
    }

    private void saveNote() {
        Note note = new Note();
        note.title = etNote.getText().toString();
        note.note = etNote.getText().toString();
        note.tag = spinnerTag.getSelectedItem().toString();
        note.priority = Integer.parseInt(spinnerPriority.getSelectedItem().toString());
        JoogarRecord.save(note);
        ((MainActivity)getActivity()).getNotes();
        dismiss();
    }

    private void prepareTag() {
        List<Tag> tagList = TagTable.find().orderBy(TagTable.name).find().toListAndClose();
        String[] tagArray = createStringArrayFromTagList(tagList);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(),
                android.R.layout.simple_spinner_item, tagArray);
        spinnerTag.setAdapter(adapter);
    }

    private void preparePriority() {

        String[] priorityArray = new String[] {"1", "2", "3", "4", "5"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(),
                android.R.layout.simple_spinner_item, priorityArray);
        spinnerPriority.setAdapter(adapter);
    }

    private String[] createStringArrayFromTagList(List<Tag> tagList) {
        String[] tags = new String[tagList.size()];
        for (int i = 0; i < tagList.size(); ++i) {
            tags[i] = tagList.get(i).name;
        }
        return tags;
    }


}
