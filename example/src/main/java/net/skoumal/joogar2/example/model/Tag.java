package net.skoumal.joogar2.example.model;

import net.skoumal.joogar2.annotations.Index;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;
import net.skoumal.joogar2.annotations.Unique;

/**
 * Created by Pavel Salva
 */

@Table()
public class Tag {
    @PrimaryKey
    public String name;
}
