package net.skoumal.joogar2.example;

import android.app.Application;

import com.facebook.stetho.Stetho;

import net.skoumal.joogar2.shared.Joogar;


/**
 * Created by Pavel Salva
 */

public class ClientApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Joogar.init(this);
        Joogar.setDebug(true);

        Joogar.getInstance().setJoogarDatabaseCustomMigrations(
                new String[] {
                        "INSERT INTO Note (title, note, priority, tag) VALUES ('Note1', 'This is note1', 1, 'tag2');",
                        "INSERT INTO Note (title, note, priority, tag) VALUES ('Note2', 'This is note2', 5, 'tag1');",
                        "INSERT INTO Note (title, note, priority, tag) VALUES ('Note3', 'This is note3', 3, 'tag2');",
                        "INSERT INTO Note (title, note, priority, tag) VALUES ('Note4', 'This is note4', 2, 'tag3');",
                        "INSERT INTO Tag (name) VALUES ('tag3');",
                        "INSERT INTO Tag (name) VALUES ('tag2');",
                        "INSERT INTO Tag (name) VALUES ('tag1');"});

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }
}
