package net.skoumal.joogar2;

import net.skoumal.joogar2.shared.util.QueryBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class QueryBuilderTests {

    @Test
    public void testNoArguments() {
        try {
            QueryBuilder.generatePlaceholders(0);
            fail("Should throw RuntimeException.");
        } catch (RuntimeException e) {
            // great!
        }
    }

    @Test
    public void testOneArgument() {
        assertEquals("?", QueryBuilder.generatePlaceholders(1));
    }

    @Test
    public void testTwoArguments() {
        assertEquals("?,?", QueryBuilder.generatePlaceholders(2));
    }

    @Test
    public void manyArgumentsTest() {
        assertEquals("?,?,?,?,?,?,?,?,?,?", QueryBuilder.generatePlaceholders(10));
    }

    @Test
    public void testLimitValidator() {
        //only limit
        assertTrue(QueryBuilder.isValidLimit("1"));
        //only limit, more digits
        assertTrue(QueryBuilder.isValidLimit("123002"));
        //only limit, negative
        assertTrue(QueryBuilder.isValidLimit("-1"));
        //only limit, negative, more digits
        assertTrue(QueryBuilder.isValidLimit("-123002"));

        //offset and limit
        assertTrue(QueryBuilder.isValidLimit("1,1"));
        //offset and limit, more digits
        assertTrue(QueryBuilder.isValidLimit("1855,11534"));
        //offset and limit, negative
        assertTrue(QueryBuilder.isValidLimit("-1,-1"));
        //offset and limit, negative, more digits
        assertTrue(QueryBuilder.isValidLimit("-1855,-11534"));
        //offset and limit, negative combination
        assertTrue(QueryBuilder.isValidLimit("-1,1"));
        //offset and limit, negative combination
        assertTrue(QueryBuilder.isValidLimit("1,-1"));

        //allow spaces
        assertTrue(QueryBuilder.isValidLimit("  1   ,   1   "));


        assertFalse(QueryBuilder.isValidLimit(""));
        assertFalse(QueryBuilder.isValidLimit("X"));
        assertFalse(QueryBuilder.isValidLimit("LIMIT 10 OFFSET 8"));
        assertFalse(QueryBuilder.isValidLimit("1 1"));
        assertFalse(QueryBuilder.isValidLimit("1 2, 3"));
        assertFalse(QueryBuilder.isValidLimit("1, 2, 3"));
    }

    @Test
    public void testOrderByValidator() {
        String[] collations = new String[]{
                "BINARY", "NOCASE", "RTRIM",    //sqlite build-in
                "LOCALIZED", "UNICODE"          //android specific
        };
        String[] orders = new String[]{"ASC", "DESC"};

        //test all combinations
        for (String collation : collations) {
            for (String order : orders) {
                //standard
                assertTrue(QueryBuilder.isValidOrderBy(
                        "COLLATE " + collation + " " + order
                ));

                //spacing
                assertTrue(QueryBuilder.isValidOrderBy(
                        "  COLLATE  " + collation + "   " + order + "   "
                ));

                //case insensitive
                assertTrue(QueryBuilder.isValidOrderBy(
                        "  collATe  " + collation.toLowerCase() + "   " + order
                ));
            }
        }

        //collation only
        for (String collation : collations) {
            //standard
            assertTrue(QueryBuilder.isValidOrderBy(
                    "COLLATE " + collation
            ));

            //spacing
            assertTrue(QueryBuilder.isValidOrderBy(
                    "  COLLATE  " + collation + "   "
            ));

            //case insensitive
            assertTrue(QueryBuilder.isValidOrderBy(
                    "  collATe  " + collation.toLowerCase()
            ));
        }

        //ordering only
        for (String order : orders) {
            //standard
            assertTrue(QueryBuilder.isValidOrderBy(
                    order
            ));

            //spacing
            assertTrue(QueryBuilder.isValidOrderBy(
                    "    " + order + "     "
            ));

            //case insensitive
            assertTrue(QueryBuilder.isValidOrderBy(
                    order.toLowerCase()
            ));
        }


        assertFalse(QueryBuilder.isValidOrderBy("A"));
        assertFalse(QueryBuilder.isValidOrderBy("ASCC"));
        assertFalse(QueryBuilder.isValidOrderBy("COLLATE"));
        assertFalse(QueryBuilder.isValidOrderBy("COLLATE ASC"));
        assertFalse(QueryBuilder.isValidOrderBy("COLLATE SOMETHING ASC"));
        assertFalse(QueryBuilder.isValidOrderBy("ASC DESC"));
        assertFalse(QueryBuilder.isValidOrderBy("COLL ATE UNICODE"));
    }
}
