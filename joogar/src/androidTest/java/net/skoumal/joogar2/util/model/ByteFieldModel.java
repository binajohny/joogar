package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class ByteFieldModel {

    @PrimaryKey
    private Long id;
    private Byte objectByte;
    private byte rawByte;

    public ByteFieldModel() {}

    public ByteFieldModel(Byte objectByte) {
        this.objectByte = objectByte;
    }

    public ByteFieldModel(byte rawByte) {
        this.rawByte = rawByte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte getObjectByte() {
        return objectByte;
    }

    public void setObjectByte(Byte objectByte) {
        this.objectByte = objectByte;
    }

    public byte getRawByte() {
        return rawByte;
    }

    public void setRawByte(byte rawByte) {
        this.rawByte = rawByte;
    }
}
