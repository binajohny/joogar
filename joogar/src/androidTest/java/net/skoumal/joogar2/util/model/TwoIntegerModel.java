package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class TwoIntegerModel {
    @PrimaryKey
    private Long id;
    private Integer int1;
    private Integer int2;

    public TwoIntegerModel() {}

    public TwoIntegerModel(Integer int1, Integer int2) {
        this.int1 = int1;
        this.int2 = int2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInt1() {
        return int1;
    }

    public void setInt1(Integer int1) {
        this.int1 = int1;
    }

    public Integer getInt2() {
        return int2;
    }

    public void setInt2(Integer int2) {
        this.int2 = int2;
    }
}
