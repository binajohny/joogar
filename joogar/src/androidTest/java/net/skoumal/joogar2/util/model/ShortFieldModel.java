package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class ShortFieldModel {

    @PrimaryKey
    private Long id;
    private Short objectShort;
    private short rawShort;

    public ShortFieldModel() {}

    public ShortFieldModel(Short objectShort) {
        this.objectShort = objectShort;
    }

    public ShortFieldModel(short rawShort) {
        this.rawShort = rawShort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getObjectShort() {
        return objectShort;
    }

    public void setObjectShort(Short objectShort) {
        this.objectShort = objectShort;
    }

    public short getRawShort() {
        return rawShort;
    }

    public void setRawShort(short rawShort) {
        this.rawShort = rawShort;
    }
}
