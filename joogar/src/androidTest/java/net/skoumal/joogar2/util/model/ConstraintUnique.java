package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;
import net.skoumal.joogar2.annotations.Unique;

/**
 * Created by Pavel Salva
 */

@Table
public class ConstraintUnique {
    @PrimaryKey
    public int id;
    @Unique
    public int uniqueField;

    public ConstraintUnique() {
    }

    public ConstraintUnique(int uniqueField) {
        this.uniqueField = uniqueField;
    }
}
