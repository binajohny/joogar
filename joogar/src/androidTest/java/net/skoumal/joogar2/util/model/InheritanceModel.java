package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class InheritanceModel extends AbstractClass2 {
    @PrimaryKey
    public Long id;
    public Integer inheritaceModelInteger;
}
