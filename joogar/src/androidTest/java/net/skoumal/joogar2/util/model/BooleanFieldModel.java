package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class BooleanFieldModel {

    @PrimaryKey
    private Long id;
    private Boolean objectBoolean;
    private boolean rawBoolean;

    public BooleanFieldModel() {}

    public BooleanFieldModel(Boolean objectBoolean) {
        this.objectBoolean = objectBoolean;
    }

    public BooleanFieldModel(boolean rawBoolean) {
        this.rawBoolean = rawBoolean;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getObjectBoolean() {
        return objectBoolean;
    }

    public void setObjectBoolean(Boolean objectBoolean) {
        this.objectBoolean = objectBoolean;
    }

    public boolean getRawBoolean() {
        return rawBoolean;
    }

    public void setRawBoolean(boolean rawBoolean) {
        this.rawBoolean = rawBoolean;
    }
}
