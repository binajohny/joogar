package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */
@Table
public class ByteArrayModel {

    @PrimaryKey
    private Long id;
    private Byte[] objectByteArray;
    private byte[] rawByteArray;

    public ByteArrayModel() {}

    public ByteArrayModel(Byte[] objectByteArray, byte[] rawByteArray) {
        this.objectByteArray = objectByteArray;
        this.rawByteArray = rawByteArray;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte[] getObjectByteArray() {
        return objectByteArray;
    }

    public void setObjectByteArray(Byte[] objectByteArray) {
        this.objectByteArray = objectByteArray;
    }

    public byte[] getRawByteArray() {
        return rawByteArray;
    }

    public void setRawByteArray(byte[] rawByteArray) {
        this.rawByteArray = rawByteArray;
    }
}
