package net.skoumal.joogar2.util;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.shared.Joogar;
import net.skoumal.joogar2.shared.JoogarDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.fail;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public abstract class CrudTestCase {

    @Before
    public void before() {
        Context context = InstrumentationRegistry.getTargetContext();

        Joogar.init(context);
    }

    @After
    public void after() {
        List<JoogarDatabase> list = Joogar.getInstance().getDatabases();

        for (JoogarDatabase jd : list) {
            jd.close();
            boolean deleted = jd.getPath().delete();
            if (!deleted) {
                fail("Cannot delete database.");
            }
        }

        Joogar.getInstance().close();
    }
}
