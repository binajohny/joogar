package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class CharacterFieldModel {

    @PrimaryKey
    private Long id;
    private Character objectCharacter;
    private char rawCharacter;

    public CharacterFieldModel() {}

    public CharacterFieldModel(Character objectCharacter) {
        this.objectCharacter = objectCharacter;
    }

    public CharacterFieldModel(char rawCharacter) {
        this.rawCharacter = rawCharacter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getObjectCharacter() {
        return objectCharacter;
    }

    public void setObjectCharacter(Character objectCharacter) {
        this.objectCharacter = objectCharacter;
    }

    public char getRawCharacter() {
        return rawCharacter;
    }

    public void setRawCharacter(char rawCharacter) {
        this.rawCharacter = rawCharacter;
    }
}

