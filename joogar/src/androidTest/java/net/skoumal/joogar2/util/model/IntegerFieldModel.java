package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class IntegerFieldModel {

    @PrimaryKey
    private Long id;
    private int rawInt;
    private Integer objectInt;

    public IntegerFieldModel() {}

    public IntegerFieldModel(int rawInt) {
        this.rawInt = rawInt;
    }

    public IntegerFieldModel(Integer objectInt) {
        this.objectInt = objectInt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRawInt() {
        return rawInt;
    }

    public void setRawInt(int rawInt) {
        this.rawInt = rawInt;
    }

    public Integer getObjectInt() {
        return objectInt;
    }

    public void setObjectInt(Integer objectInt) {
        this.objectInt = objectInt;
    }
}
