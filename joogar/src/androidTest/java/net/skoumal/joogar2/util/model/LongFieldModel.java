package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class LongFieldModel {

    @PrimaryKey
    private Long id;
    private Long objectLong;
    private long rawLong;

    public LongFieldModel() {}

    public LongFieldModel(Long objectLong) {
        this.objectLong = objectLong;
    }

    public LongFieldModel(long rawLong) {
        this.rawLong = rawLong;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectLong() {
        return objectLong;
    }

    public void setObjectLong(Long objectLong) {
        this.objectLong = objectLong;
    }

    public long getRawLong() {
        return rawLong;
    }

    public void setRawLong(long rawLong) {
        this.rawLong = rawLong;
    }
}
