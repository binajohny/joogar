package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Jan Bína on 4/13/17.
 */

@Table
public class ComposedModel {

    @PrimaryKey
    private Long id;
    private boolean active;
    private String name;
    private long created;
    private int size;
    private double floating;

    public ComposedModel() {
    }

    public ComposedModel(boolean active, String name, long created, int size, double floating) {
        this.active = active;
        this.name = name;
        this.created = created;
        this.size = size;
        this.floating = floating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getFloating() {
        return floating;
    }

    public void setFloating(double floating) {
        this.floating = floating;
    }
}
