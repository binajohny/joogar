package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.NonNull;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class ConstraintNonnull {
    @PrimaryKey
    public int id;
    @NonNull
    public Integer nonnullField;
}
