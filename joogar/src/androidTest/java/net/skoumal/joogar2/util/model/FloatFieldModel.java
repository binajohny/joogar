package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class FloatFieldModel {

    @PrimaryKey
    private Long id;
    private Float objectFloat;
    private float rawFloat;

    public FloatFieldModel() {}

    public FloatFieldModel(Float objectFloat) {
        this.objectFloat = objectFloat;
    }

    public FloatFieldModel(float rawFloat) {
        this.rawFloat = rawFloat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getObjectFloat() {
        return objectFloat;
    }

    public void setObjectFloat(Float objectFloat) {
        this.objectFloat = objectFloat;
    }

    public float getRawFloat() {
        return rawFloat;
    }

    public void setRawFloat(float rawFloat) {
        this.rawFloat = rawFloat;
    }
}
