package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class ConstraintPK {
    @PrimaryKey
    public int id;
    public int value;

    public ConstraintPK() {
    }

    public ConstraintPK(int value) {
        this.value = value;
    }
}
