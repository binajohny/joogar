package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */
@Table
public class StringFieldModel {

    @PrimaryKey
    public Long id;
    public String string;

    public StringFieldModel() {}

    public StringFieldModel(String string) {
        this.string = string;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
