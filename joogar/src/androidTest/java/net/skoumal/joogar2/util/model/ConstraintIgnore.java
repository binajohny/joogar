package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.Ignore;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class ConstraintIgnore {
    @PrimaryKey
    public int id;

    @Ignore
    public int ignoredField;
}
