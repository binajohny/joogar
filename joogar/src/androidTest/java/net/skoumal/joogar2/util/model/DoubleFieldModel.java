package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */

@Table
public class DoubleFieldModel {

    @PrimaryKey
    private Long id;
    private Double objectDouble;
    private double rawDouble;

    public DoubleFieldModel() {}

    public DoubleFieldModel(Double objectDouble) {
        this.objectDouble = objectDouble;
    }

    public DoubleFieldModel(double rawDouble) {
        this.rawDouble = rawDouble;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getObjectDouble() {
        return objectDouble;
    }

    public void setObjectDouble(Double objectDouble) {
        this.objectDouble = objectDouble;
    }

    public double getRawDouble() {
        return rawDouble;
    }

    public void setRawDouble(double rawDouble) {
        this.rawDouble = rawDouble;
    }
}
