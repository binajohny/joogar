package net.skoumal.joogar2.util.model;

import net.skoumal.joogar2.annotations.NonNull;
import net.skoumal.joogar2.annotations.PrimaryKey;
import net.skoumal.joogar2.annotations.Table;

/**
 * Created by Pavel Salva
 */
@Table
public class CompositePkModel {
    @PrimaryKey @NonNull
    public int id1;
    @PrimaryKey @NonNull
    public int id2;

    public CompositePkModel() {
    }

    public CompositePkModel(int id1, int id2) {
        this.id1 = id1;
        this.id2 = id2;
    }
}
