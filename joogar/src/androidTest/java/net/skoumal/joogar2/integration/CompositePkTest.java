package net.skoumal.joogar2.integration;

import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.CompositePkModel;
import net.skoumal.joogar2.util.model.CompositePkModelTable;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Jan Bína on 5/13/17.
 */

@RunWith(AndroidJUnit4.class)
public class CompositePkTest extends CrudTestCase {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testInsertDifferentPks() {
        for (int i = 0; i < 10; i++) {
            JoogarRecord.insert(new CompositePkModel(0, i));
        }
        for (int i = 0; i < 10; i++) {
            JoogarRecord.insert(new CompositePkModel(i, 10));
        }

        assertEquals(20, CompositePkModelTable.countAll());
    }

    @Test
    public void testInsertSamePk() {
        JoogarRecord.insert(new CompositePkModel(1, 1));
        JoogarRecord.insert(new CompositePkModel(1, 2));

        thrown.expect(SQLiteConstraintException.class);
        thrown.expectMessage("UNIQUE constraint failed");

        JoogarRecord.insert(new CompositePkModel(1, 1));
    }

    @Test
    public void testFindByPrimaryKey() {
        for (int i = 0; i < 10; i++) {
            JoogarRecord.insert(new CompositePkModel(0, i));
        }

        CompositePkModel model = CompositePkModelTable.findByPrimaryKey(0, 5);
        assertNotNull(model);
        assertEquals(0, model.id1);
        assertEquals(5, model.id2);
    }
}
