package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.StringFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.StringFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class StringFieldTests extends CrudTestCase {

    @Test
    public void testNullString() {
        JoogarRecord.save(new StringFieldModel());
        StringFieldModel model = StringFieldModelTable.findById(1L);
        assertNull(model.getString());
    }

    @Test
    public void testString() {
        String string = "Test String";
        JoogarRecord.save(new StringFieldModel(string));
        StringFieldModel model = StringFieldModelTable.findById(1L);
        assertEquals(string, model.getString());
    }
}
