package net.skoumal.joogar2.test.advanced;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.SimpleModel;
import net.skoumal.joogar2.util.model.SimpleModelTable;
import net.skoumal.joogar2.util.model.TwoIntegerModel;
import net.skoumal.joogar2.util.model.TwoIntegerModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class DatabaseTests extends CrudTestCase {

    @Test
    public void testEmptyDatabase() throws Exception {
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testOneSave() throws Exception {
        JoogarRecord.save(new SimpleModel());
        assertEquals(1L, SimpleModelTable.countAll());
    }

    @Test
    public void testTwoSave() throws Exception {
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        assertEquals(2L, SimpleModelTable.countAll());
    }

    @Test
    public void testManySave() throws Exception {
        for (int i = 1; i <= 100; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        assertEquals(100L, SimpleModelTable.countAll());
    }

    @Test
    public void testDefaultId() throws Exception {
        assertEquals(1L, JoogarRecord.save(new SimpleModel()));
    }

    @Test
    public void testWhereCount() throws Exception {
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        assertEquals(1L, SimpleModelTable.count(SimpleModelTable.where().id().eq(1L)));
    }

    @Test
    public void testWhereNoCount() throws Exception {
        assertEquals(0L, SimpleModelTable.count(SimpleModelTable.where().id().eq(1L)));
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        assertEquals(0L, SimpleModelTable.count(SimpleModelTable.where().id().eq(3L)));
        assertEquals(0L, SimpleModelTable.count(SimpleModelTable.where().id().eq(4L)));
    }

    @Test
    public void testDelete() throws Exception {
        SimpleModel model = new SimpleModel();
        JoogarRecord.save(model);
        assertEquals(1L, SimpleModelTable.countAll());
        assertTrue(JoogarRecord.delete(model));
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteUnsaved() throws Exception {
        SimpleModel model = new SimpleModel();
        assertEquals(0L, SimpleModelTable.countAll());
        assertFalse(JoogarRecord.delete(model));
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteWrong() throws Exception {
        SimpleModel model = new SimpleModel();
        JoogarRecord.save(model);
        assertEquals(1L, SimpleModelTable.countAll());
        model.id = Long.MAX_VALUE;
        assertFalse(JoogarRecord.delete(model));
        assertEquals(1L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteAll() throws Exception {
        int elementNumber = 100;
        for (int i = 0; i < elementNumber; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        assertEquals(elementNumber, SimpleModelTable.deleteAll());
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteAllWhere() throws Exception {
        int elementNumber = 100;
        for (int i = 0; i < elementNumber; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        assertEquals(elementNumber - 1, SimpleModelTable.delete(SimpleModelTable.where().id().gt(1L)));
        assertEquals(1L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteInTransactionFew() throws Exception {
        SimpleModel first = new SimpleModel();
        SimpleModel second = new SimpleModel();
        SimpleModel third = new SimpleModel();
        JoogarRecord.save(first);
        JoogarRecord.save(second);
        // Not saving last model
        assertEquals(2L, SimpleModelTable.countAll());
        assertEquals(2, SimpleModelTable.deleteInTx(first, second, third));
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testDeleteInTransactionMany() throws Exception {
        long elementNumber = 100;
        List<SimpleModel> models = new ArrayList<>();
        for (int i = 0; i < elementNumber; i++) {
            SimpleModel model = new SimpleModel();
            models.add(model);
            // Not saving last model
            if (i < elementNumber - 1) {
                JoogarRecord.save(model);
            }
        }
        assertEquals(elementNumber - 1, SimpleModelTable.countAll());
        assertEquals(elementNumber - 1, SimpleModelTable.deleteInTx(models));
        assertEquals(0L, SimpleModelTable.countAll());
    }

    @Test
    public void testSaveInTransaction() throws Exception {
        SimpleModelTable.saveInTx(new SimpleModel(), new SimpleModel());
        assertEquals(2L, SimpleModelTable.countAll());
    }

    @Test
    public void testListAll() throws Exception {
        long elementNumber = 100;
        for (int i = 0; i < elementNumber; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        List<SimpleModel> models = SimpleModelTable.find().find().toListAndClose();
        assertEquals(elementNumber, models.size());
        for (int i = 0; i < elementNumber; i++) {
            assertEquals(Long.valueOf(i + 1), models.get(i).id);
        }
    }

    @Test
    public void testFind() throws Exception {
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        List<SimpleModel> models =
                SimpleModelTable.find().where(SimpleModelTable.where().id().eq(2L)).find().toListAndClose();
        assertEquals(1, models.size());
        assertEquals(Long.valueOf(2L), models.get(0).id);
    }

    @Test
    public void testFindById() throws Exception {
        JoogarRecord.save(new SimpleModel());
        assertEquals(Long.valueOf(1L), SimpleModelTable.findById(1L).id);
    }

    @Test
    public void testFindByIdTwo() throws Exception {
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        JoogarRecord.save(new SimpleModel());
        List<SimpleModel> models =
                SimpleModelTable.find().where(SimpleModelTable.where().id().in(1L, 3L)).find().toListAndClose();
        assertEquals(2, models.size());
        assertEquals(Long.valueOf(1L), models.get(0).id);
        assertEquals(Long.valueOf(3L), models.get(1).id);
    }

    @Test
    public void testFindByIdMany() throws Exception {
        for (int i = 0; i < 10; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        List<SimpleModel> models =
                SimpleModelTable.find().where(SimpleModelTable.where().id().in(1L, 3L, 6L, 10L)).find().toListAndClose();
        assertEquals(4, models.size());
        assertEquals(Long.valueOf(1L), models.get(0).id);
        assertEquals(Long.valueOf(3L), models.get(1).id);
        assertEquals(Long.valueOf(6L), models.get(2).id);
        assertEquals(Long.valueOf(10L), models.get(3).id);
    }

    @Test
    public void testFindByIdOrder() throws Exception {
        for (int i = 0; i < 10; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        List<SimpleModel> models =
                SimpleModelTable.find().where(SimpleModelTable.where().id().in(10L, 6L, 3L, 1L)).find().toListAndClose();
        assertEquals(4, models.size());
        // The order of the query doesn't matter
        assertEquals(Long.valueOf(1L), models.get(0).id);
        assertEquals(Long.valueOf(3L), models.get(1).id);
        assertEquals(Long.valueOf(6L), models.get(2).id);
        assertEquals(Long.valueOf(10L), models.get(3).id);
    }

    @Test
    public void testFindByIdNull() throws Exception {
        JoogarRecord.save(new SimpleModel());
        assertNull(SimpleModelTable.findById(2L));
    }

    @Test
    public void testFindAll() throws Exception {
        for (int i = 0; i < 100; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        Iterator<SimpleModel> cursor = SimpleModelTable.find().find().iterator();
        for (int i = 0; i < 100; i++) {
            assertTrue(cursor.hasNext());
            SimpleModel model = cursor.next();
            assertNotNull(model);
            assertEquals(Long.valueOf(i + 1), model.id);
        }
    }

    @Test
    public void testFindAsIterator() throws Exception {
        for (int i = 0; i < 100; i++) {
            JoogarRecord.save(new SimpleModel());
        }
        Iterator<SimpleModel> cursor = SimpleModelTable.find()
                .where(SimpleModelTable.where().id().gtEq(50L)).find().iterator();
        for (int i = 50; i <= 100; i++) {
            assertTrue(cursor.hasNext());
            SimpleModel model = cursor.next();
            assertNotNull(model);
            assertEquals(Long.valueOf(i), model.id);
        }
    }

    @Test
    public void testFindAsIteratorOutOfBounds() throws Exception {
        JoogarRecord.save(new SimpleModel());
        Iterator<SimpleModel> cursor = SimpleModelTable.find()
                .where(SimpleModelTable.where().id().eq(1L)).find().iterator();
        assertTrue(cursor.hasNext());
        SimpleModel model = cursor.next();
        assertNotNull(model);
        assertEquals(Long.valueOf(1), model.id);

        model = cursor.next();
        assertNull(model);
    }

    @Test
    public void testDisallowRemoveCursor() throws Exception {
        JoogarRecord.save(new SimpleModel());
        Iterator<SimpleModel> cursor = SimpleModelTable.find()
                .where(SimpleModelTable.where().id().eq(1L)).find().iterator();
        assertTrue(cursor.hasNext());
        SimpleModel model = cursor.next();
        assertNotNull(model);
        assertEquals(Long.valueOf(1), model.id);

        try {
            cursor.remove();
            fail("UnsupportedOperationException should be thrown.");
        } catch (UnsupportedOperationException e) {
            // This is correct behaviour
        }
    }

    @Test
    public void testIdSetAutomatically() {
        SimpleModel model = new SimpleModel();

        Long id = JoogarRecord.save(model);

        assertEquals(model.id, id);
    }

    @Test
    public void testSaveInsertUpdate() {
        TwoIntegerModel twoIntegerModel = new TwoIntegerModel(1, 2);

        // save new twoIntegerModel
        JoogarRecord.save(twoIntegerModel);
        assertEquals(TwoIntegerModelTable.countAll(), 1);

        TwoIntegerModelTable.deleteAll();

        // insert new twoIntegerModel
        JoogarRecord.insert(twoIntegerModel);
        assertEquals(TwoIntegerModelTable.countAll(), 1);

        TwoIntegerModelTable.deleteAll();

        // try to update not existing twoIntegerModel
        JoogarRecord.update(twoIntegerModel);
        assertEquals(TwoIntegerModelTable.countAll(), 0);


        long id = JoogarRecord.insert(twoIntegerModel);

        // save changed twoIntegerModel, should update existing
        twoIntegerModel.setInt1(10);
        JoogarRecord.save(twoIntegerModel);
        assertEquals(TwoIntegerModelTable.countAll(), 1);
        assertEquals(TwoIntegerModelTable.findById(id).getInt1().longValue(), 10);

        // insert changed twoIntegerModel, should throw exception
        twoIntegerModel.setInt1(20);
        try {
            JoogarRecord.insert(twoIntegerModel);
            fail("Should throw ConstraintException");
        } catch (SQLiteConstraintException e) {
            //desired behavior
        }

        // update changed twoIntegerModel, should update existing
        twoIntegerModel.setInt1(30);
        JoogarRecord.update(twoIntegerModel);
        assertEquals(TwoIntegerModelTable.countAll(), 1);
        assertEquals(TwoIntegerModelTable.findById(id).getInt1().longValue(), 30);
    }

    @Test
    public void testCountWhere() {
        List<TwoIntegerModel> models = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (int j = 10; j < 20; j++) {
                models.add(new TwoIntegerModel(i, j));
            }
        }

        Collections.shuffle(models);

        TwoIntegerModelTable.insertInTx(models);

        assertEquals(0, TwoIntegerModelTable.count(TwoIntegerModelTable.where().int1().lt(0)));
        assertEquals(0, TwoIntegerModelTable.count(TwoIntegerModelTable.where().int1().gt(9)));

        assertEquals(3*10, TwoIntegerModelTable.count(TwoIntegerModelTable.where().int1().lt(3)));

        assertEquals(3, TwoIntegerModelTable.count(TwoIntegerModelTable.where().int1().lt(3).and().int2().eq(10)));

        assertEquals(3*10 + 10 - 3, TwoIntegerModelTable.count(TwoIntegerModelTable.where().int1().lt(3).or().int2().eq(10)));
    }

    @Test
    public void testUpdateAll() {
        for (int i = 0; i < 50; i++) {
            JoogarRecord.insert(new TwoIntegerModel(i, 0));
        }

        ContentValues values = new ContentValues();
        values.put(TwoIntegerModelTable.int1.name(), 0);

        int updated = TwoIntegerModelTable.update(values);

        assertEquals(50, updated);

        List<TwoIntegerModel> list = TwoIntegerModelTable.find().orderBy(TwoIntegerModelTable.int1).find().toListAndClose();

        for (int i = 0; i < 50; i++) {
            assertEquals(0, list.get(i).getInt1().intValue());
        }
    }

    @Test
    public void testUpdateWhere() {
        for (int i = 0; i < 50; i++) {
            JoogarRecord.insert(new TwoIntegerModel(i, 0));
        }

        ContentValues values = new ContentValues();
        values.put(TwoIntegerModelTable.int1.name(), -1);

        int updated = TwoIntegerModelTable.update(values, TwoIntegerModelTable.where().int1().lt(10));

        assertEquals(10, updated);

        List<TwoIntegerModel> list = TwoIntegerModelTable.find().orderBy(TwoIntegerModelTable.int1).find().toListAndClose();

        for (int i = 0; i < 10; i++) {
            assertEquals(-1, list.get(i).getInt1().intValue());
        }
    }
}