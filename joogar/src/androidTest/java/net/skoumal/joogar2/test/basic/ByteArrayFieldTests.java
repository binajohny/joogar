package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.Utils;
import net.skoumal.joogar2.util.model.ByteArrayModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.ByteArrayModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class ByteArrayFieldTests extends CrudTestCase {

    @Test
    public void testNullByteArrayField() {
        JoogarRecord.save(new ByteArrayModel(null, null));
        ByteArrayModel model = ByteArrayModelTable.findById(1L);
        assertNull(model.getObjectByteArray());
        assertNull(model.getRawByteArray());
    }

    @Test
    public void testEmptyByteArray() {
        byte[] array = "".getBytes();
        Byte[] objectArray = Utils.toObject(array);

        JoogarRecord.save(new ByteArrayModel(objectArray, array));
        ByteArrayModel model = ByteArrayModelTable.findById(1L);
        assertEquals(new String(array), new String(model.getRawByteArray()));
        assertTrue(Arrays.equals(array, model.getRawByteArray()));
        assertTrue(Arrays.equals(objectArray, model.getObjectByteArray()));
    }

    @Test
    public void testByteArray() {
        byte[] array = "hello".getBytes();
        Byte[] objectArray = Utils.toObject(array);

        JoogarRecord.save(new ByteArrayModel(objectArray, array));
        ByteArrayModel model = ByteArrayModelTable.findById(1L);
        assertEquals(new String(array), new String(model.getRawByteArray()));
        assertTrue(Arrays.equals(array, model.getRawByteArray()));
        assertTrue(Arrays.equals(objectArray, model.getObjectByteArray()));
    }
}
