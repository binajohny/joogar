package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.IntegerFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.IntegerFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class IntegerFieldTests extends CrudTestCase {

    @Test
    public void testNullInteger() {
        JoogarRecord.save(new IntegerFieldModel());
        IntegerFieldModel model = IntegerFieldModelTable.findById(1L);
        assertNull(model.getObjectInt());
    }

    @Test
    public void testNullInt() {
        JoogarRecord.save(new IntegerFieldModel());
        IntegerFieldModel model = IntegerFieldModelTable.findById(1L);
        assertEquals(0, model.getRawInt());
    }

    @Test
    public void testInteger() {
        Integer integer = 25;
        JoogarRecord.save(new IntegerFieldModel(integer));
        IntegerFieldModel model = IntegerFieldModelTable.findById(1L);
        assertEquals(integer, model.getObjectInt());
    }

    @Test
    public void testInt() {
        JoogarRecord.save(new IntegerFieldModel(25));
        IntegerFieldModel model = IntegerFieldModelTable.findById(1L);
        assertEquals(25, model.getRawInt());
    }
}
