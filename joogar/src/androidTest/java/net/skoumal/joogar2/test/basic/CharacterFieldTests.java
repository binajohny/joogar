package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.CharacterFieldModel;
import net.skoumal.joogar2.util.model.CharacterFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class CharacterFieldTests extends CrudTestCase {

    @Test
    public void testNullCharacter() {
        JoogarRecord.save(new CharacterFieldModel());
        CharacterFieldModel model = CharacterFieldModelTable.findById(1L);
        assertNull(model.getObjectCharacter());
    }

    @Test
    public void testNullRawCharacter() {
        JoogarRecord.save(new CharacterFieldModel());
        CharacterFieldModel model = CharacterFieldModelTable.findById(1L);
        assertEquals(0, model.getRawCharacter());
    }

    @Test
    public void testObjectCharacter() {
        Character objectCharacter = 'X';
        JoogarRecord.save(new CharacterFieldModel(objectCharacter));
        CharacterFieldModel model = CharacterFieldModelTable.findById(1L);
        assertEquals(objectCharacter, model.getObjectCharacter());
    }

    @Test
    public void testRawCharacter() {
        JoogarRecord.save(new CharacterFieldModel('X'));
        CharacterFieldModel model = CharacterFieldModelTable.findById(1L);
        assertEquals('X', model.getRawCharacter());
    }
}
