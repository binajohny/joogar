package net.skoumal.joogar2.test.advanced;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.shared.cursor.JoogarCursor;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.StringFieldModel;
import net.skoumal.joogar2.util.model.StringFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class BulkSaveTests extends CrudTestCase {

    @Test
    public void testStringCollectionBulkSave() {
        List<StringFieldModel> modelList = prepareEntityList();

        StringFieldModelTable.saveInTx(modelList);
        checkEntityList();

        StringFieldModelTable.deleteAll();

        StringFieldModelTable.insertInTx(modelList);
        checkEntityList();
    }

    private void checkEntityList() {
        JoogarCursor<StringFieldModel> cursor = StringFieldModelTable.find().find();

        int index = 0;
        for (StringFieldModel sf : cursor) {
            assertEquals(sf.getString(), getStringForIndex(index));

            index ++;
        }

        cursor.close();

        assertTrue(cursor.isClosed());
    }

    private List<StringFieldModel> prepareEntityList() {
        List<StringFieldModel> modelList = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {
            modelList.add(new StringFieldModel(getStringForIndex(i)));
        }
        return modelList;
    }

    private String getStringForIndex(int gIndex) {
        return "abc" + gIndex;
    }
}
