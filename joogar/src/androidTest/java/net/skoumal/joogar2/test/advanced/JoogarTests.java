package net.skoumal.joogar2.test.advanced;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.shared.Joogar;
import net.skoumal.joogar2.shared.JoogarDatabase;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class JoogarTests {

    private Context context = InstrumentationRegistry.getTargetContext();

    @Test
    public void testCloseOneDBAndDeleteFile() {
        Joogar.init(context);

        File dbFile = Joogar.getInstance().getDatabases().get(0).getPath();

        Joogar.getInstance().close();

        assertTrue(dbFile.delete());
    }

    @Test
    public void testUninitialized() {
        assertNull(Joogar.getInstance());
    }

    @Test
    public void testReInit() {
        Joogar.init(context);
        try {
            Joogar.init(context);
            fail("Should throw IllegalStateException");
        } catch (IllegalStateException e) {
            // desired behaviour
        }
    }

    @After
    public void after() throws Exception {
        if (Joogar.getInstance() != null) {
            List<JoogarDatabase> list = Joogar.getInstance().getDatabases();

            for (JoogarDatabase jd : list) {
                assertTrue(jd.getPath().delete());
            }

            Joogar.getInstance().close();
        }
    }
}
