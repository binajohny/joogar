package net.skoumal.joogar2.test.advanced;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.shared.cursor.JoogarCursor;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.SimpleModel;
import net.skoumal.joogar2.util.model.SimpleModelTable;
import net.skoumal.joogar2.util.model.TwoIntegerModel;
import net.skoumal.joogar2.util.model.TwoIntegerModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class FindStatementTests extends CrudTestCase {

    @Test
    public void testLimitAndOffset() {
        for (int i = 0; i < 50; i++) {
            JoogarRecord.save(new SimpleModel());
        }

        JoogarCursor<SimpleModel> cursor;

        cursor = SimpleModelTable.find().limit(10).find();

        assertEquals(cursor.size(), 10);

        for (int i = 0; i < 10; i++) {
            assertEquals(cursor.next().id.longValue(), i + 1);
        }

        cursor.close();

        cursor = SimpleModelTable.find().offset(40).find();

        assertEquals(cursor.size(), 10);

        for (int i = 0; i < 10; i++) {
            assertEquals(cursor.next().id.longValue(), i + 1 + 40);
        }

        cursor.close();

        cursor = SimpleModelTable.find().limit(10).offset(30).find();

        assertEquals(cursor.size(), 10);

        for (int i = 0; i < 10; i++) {
            assertEquals(cursor.next().id.longValue(), i + 1 + 30);
        }

        cursor.close();
    }

    @Test
    public void testOrderBy() {
        List<TwoIntegerModel> entities = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                entities.add(new TwoIntegerModel(i, j));
            }
        }

        Collections.shuffle(entities);

        TwoIntegerModelTable.saveInTx(entities);

        JoogarCursor<TwoIntegerModel> cursor;

        //Default ordering should be ascending
        cursor = TwoIntegerModelTable.find().orderBy(TwoIntegerModelTable.int1).find();

        assertEquals(cursor.size(), 50);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                assertEquals(cursor.next().getInt1().longValue(), i);
            }
        }

        cursor.close();

        //Explicitly ascending
        cursor = TwoIntegerModelTable.find().orderBy(TwoIntegerModelTable.int1, "ASC").find();

        assertEquals(cursor.size(), 50);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                assertEquals(cursor.next().getInt1().longValue(), i);
            }
        }

        cursor.close();

        //Descending
        cursor = TwoIntegerModelTable.find().orderBy(TwoIntegerModelTable.int1, "DESC").find();

        assertEquals(cursor.size(), 50);

        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 5; j++) {
                assertEquals(cursor.next().getInt1().longValue(), i);
            }
        }

        cursor.close();

        //Both ascending
        cursor = TwoIntegerModelTable.find()
                .orderBy(TwoIntegerModelTable.int1, "ASC")
                .orderBy(TwoIntegerModelTable.int2, "ASC")
                .find();

        assertEquals(cursor.size(), 50);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                assertEquals(cursor.next().getInt1().longValue(), i);
                assertEquals(cursor.current().getInt2().longValue(), j);
            }
        }

        cursor.close();

        //Ascending and descending
        cursor = TwoIntegerModelTable.find()
                .orderBy(TwoIntegerModelTable.int1, "ASC")
                .orderBy(TwoIntegerModelTable.int2, "DESC")
                .find();

        assertEquals(cursor.size(), 50);

        for (int i = 0; i < 10; i++) {
            for (int j = 4; j >= 0; j--) {
                assertEquals(cursor.next().getInt1().longValue(), i);
                assertEquals(cursor.current().getInt2().longValue(), j);
            }
        }

        cursor.close();

        //Both descending
        cursor = TwoIntegerModelTable.find()
                .orderBy(TwoIntegerModelTable.int1, "DESC")
                .orderBy(TwoIntegerModelTable.int2, "DESC")
                .find();

        assertEquals(cursor.size(), 50);

        for (int i = 9; i >= 0; i--) {
            for (int j = 4; j >= 0; j--) {
                assertEquals(cursor.next().getInt1().longValue(), i);
                assertEquals(cursor.current().getInt2().longValue(), j);
            }
        }

        cursor.close();

        //Descending and ascending
        cursor = TwoIntegerModelTable.find()
                .orderBy(TwoIntegerModelTable.int1, "DESC")
                .orderBy(TwoIntegerModelTable.int2, "ASC")
                .find();

        assertEquals(cursor.size(), 50);

        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 5; j++) {
                assertEquals(cursor.next().getInt1().longValue(), i);
                assertEquals(cursor.current().getInt2().longValue(), j);
            }
        }

        cursor.close();
    }
}
