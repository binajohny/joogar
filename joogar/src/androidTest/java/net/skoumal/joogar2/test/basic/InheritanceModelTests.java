package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.InheritanceModel;
import net.skoumal.joogar2.util.model.InheritanceModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class InheritanceModelTests extends CrudTestCase {

    @Test
    public void testNullValues() {
        JoogarRecord.save(new InheritanceModel());

        InheritanceModel model = InheritanceModelTable.find().first();

        assertNull(model.inheritaceModelInteger);
        assertNull(model.abstractClass2Integer);
        assertNull(model.abstractClassInteger);
    }

    @Test
    public void testNonNullValues() {
        InheritanceModel model = new InheritanceModel();
        model.abstractClassInteger = 1;
        model.abstractClass2Integer = 2;
        model.inheritaceModelInteger = 3;

        JoogarRecord.save(model);

        model = InheritanceModelTable.find().first();

        assertEquals(model.abstractClassInteger.longValue(), 1);
        assertEquals(model.abstractClass2Integer.longValue(), 2);
        assertEquals(model.inheritaceModelInteger.longValue(), 3);
    }
}
