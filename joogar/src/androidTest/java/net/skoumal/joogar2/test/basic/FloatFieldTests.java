package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.FloatFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.FloatFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class FloatFieldTests extends CrudTestCase {

    @Test
    public void testNullFloat() {
        JoogarRecord.save(new FloatFieldModel());
        FloatFieldModel model = FloatFieldModelTable.findById(1L);
        assertNull(model.getObjectFloat());
    }

    @Test
    public void testNullRawFloat() {
        JoogarRecord.save(new FloatFieldModel());
        FloatFieldModel model = FloatFieldModelTable.findById(1L);
        assertEquals(0F, model.getRawFloat(), 0.0000000001F);
    }

    @Test
    public void testObjectFloat() {
        Float objectFloat = 25F;
        JoogarRecord.save(new FloatFieldModel(objectFloat));
        FloatFieldModel model = FloatFieldModelTable.findById(1L);
        assertEquals(objectFloat, model.getObjectFloat());
    }

    @Test
    public void testRawFloat() {
        JoogarRecord.save(new FloatFieldModel(25F));
        FloatFieldModel model = FloatFieldModelTable.findById(1L);
        assertEquals(25F, model.getRawFloat(), 0.0000000001F);
    }
}
