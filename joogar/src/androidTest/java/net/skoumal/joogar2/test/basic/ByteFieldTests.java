package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.ByteFieldModel;
import net.skoumal.joogar2.util.model.ByteFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class ByteFieldTests extends CrudTestCase {

    @Test
    public void testNullByte() {
        JoogarRecord.save(new ByteFieldModel());
        ByteFieldModel model = ByteFieldModelTable.findById(1L);
        assertNull(model.getObjectByte());
    }

    @Test
    public void testNullRawByte() {
        JoogarRecord.save(new ByteFieldModel());
        ByteFieldModel model = ByteFieldModelTable.findById(1L);
        assertEquals(0, model.getRawByte());
    }

    @Test
    public void testObjectByte() {
        Byte objectByte = 10;
        JoogarRecord.save(new ByteFieldModel(objectByte));
        ByteFieldModel model = ByteFieldModelTable.findById(1L);
        assertEquals(objectByte, model.getObjectByte());
    }

    @Test
    public void testRawByte() {
        JoogarRecord.save(new ByteFieldModel((byte)10));
        ByteFieldModel model = ByteFieldModelTable.findById(1L);
        assertEquals(10, model.getRawByte());
    }
}
