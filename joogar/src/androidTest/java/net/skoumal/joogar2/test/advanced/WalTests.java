package net.skoumal.joogar2.test.advanced;

import android.os.Build;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.StringFieldModel;
import net.skoumal.joogar2.util.model.StringFieldModelTable;
import net.skoumal.joogar2.util.CrudTestCase;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class WalTests extends CrudTestCase {

    @Test
    @SdkSuppress(minSdkVersion = Build.VERSION_CODES.HONEYCOMB)
    public void testParallelReadDuringTransaction() {
        final String preTransactionString = "Pre-transaction String";
        StringFieldModel preTransactionModel = new StringFieldModel(preTransactionString);
        final long preTransactionId = JoogarRecord.save(preTransactionModel);

        StringFieldModelTable.getDatabase().beginTransaction();

        String string = "Test String";
        StringFieldModel model = StringFieldModelTable.findById(preTransactionId);
        model.setString(string);
        long id = JoogarRecord.save(model);

        Thread readThread = new Thread(new Runnable() {
            @Override
            public void run() {
                StringFieldModel query = StringFieldModelTable.findById(preTransactionId);
                assertEquals(preTransactionString, query.getString());
            }
        });
        readThread.start();
        try {
            readThread.join(1000);
        } catch (InterruptedException e) {
            fail("Join was interrupted.");
        }

        assertFalse(readThread.isAlive());

        StringFieldModelTable.getDatabase().setTransactionSuccessful();
        StringFieldModelTable.getDatabase().endTransaction();

        StringFieldModel query = StringFieldModelTable.findById(id);
        assertEquals(string, query.getString());
    }
}
