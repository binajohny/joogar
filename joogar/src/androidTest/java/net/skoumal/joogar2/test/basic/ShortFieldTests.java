package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.ShortFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.ShortFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class ShortFieldTests extends CrudTestCase {

    @Test
    public void testNullShort() {
        JoogarRecord.save(new ShortFieldModel());
        ShortFieldModel model = ShortFieldModelTable.findById(1L);
        assertNull(model.getObjectShort());
    }

    @Test
    public void testNullRawShort() {
        JoogarRecord.save(new ShortFieldModel());
        ShortFieldModel model = ShortFieldModelTable.findById(1L);
        assertEquals((short) 0, model.getRawShort());
    }

    @Test
    public void testObjectShort() {
        Short objectShort = 25;
        JoogarRecord.save(new ShortFieldModel(objectShort));
        ShortFieldModel model = ShortFieldModelTable.findById(1L);
        assertEquals(objectShort, model.getObjectShort());
    }

    @Test
    public void testRawShort() {
        JoogarRecord.save(new ShortFieldModel((short) 25));
        ShortFieldModel model = ShortFieldModelTable.findById(1L);
        assertEquals((short) 25, model.getRawShort());
    }
}
