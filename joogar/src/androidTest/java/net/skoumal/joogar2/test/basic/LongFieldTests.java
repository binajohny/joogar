package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.LongFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.LongFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class LongFieldTests extends CrudTestCase {

    @Test
    public void testNullLong() {
        JoogarRecord.save(new LongFieldModel());
        LongFieldModel model = LongFieldModelTable.findById(1L);
        assertNull(model.getObjectLong());
    }

    @Test
    public void testNullRawLong() {
        JoogarRecord.save(new LongFieldModel());
        LongFieldModel model = LongFieldModelTable.findById(1L);
        assertEquals(0L, model.getRawLong());
    }

    @Test
    public void testObjectLong() {
        Long objectLong = 25L;
        JoogarRecord.save(new LongFieldModel(objectLong));
        LongFieldModel model = LongFieldModelTable.findById(1L);
        assertEquals(objectLong, model.getObjectLong());
    }

    @Test
    public void testRawLong() {
        JoogarRecord.save(new LongFieldModel(25L));
        LongFieldModel model = LongFieldModelTable.findById(1L);
        assertEquals(25L, model.getRawLong());
    }
}
