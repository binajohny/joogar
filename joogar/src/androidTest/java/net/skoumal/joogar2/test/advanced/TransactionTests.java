package net.skoumal.joogar2.test.advanced;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.util.model.StringFieldModel;
import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.StringFieldModelTable;
import net.skoumal.joogar2.util.CrudTestCase;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class TransactionTests extends CrudTestCase {

    @Test
    public void testCommitTransaction() {
        StringFieldModelTable.getDatabase().beginTransaction();

        String string = "Test String";
        StringFieldModel model = new StringFieldModel(string);
        long id = JoogarRecord.save(model);

        StringFieldModelTable.getDatabase().setTransactionSuccessful();
        StringFieldModelTable.getDatabase().endTransaction();

        StringFieldModel query = StringFieldModelTable.findById(id);
        assertEquals(string, query.getString());
    }

    @Test
    public void testRollbackTransaction() {
        StringFieldModelTable.getDatabase().beginTransaction();

        String string = "Test String";
        StringFieldModel model = new StringFieldModel(string);
        long id = JoogarRecord.save(model);

        StringFieldModelTable.getDatabase().rollbackTransaction();

        StringFieldModel query = StringFieldModelTable.findById(id);
        assertNull(query);
    }

}
