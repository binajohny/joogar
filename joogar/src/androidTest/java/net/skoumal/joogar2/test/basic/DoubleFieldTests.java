package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.DoubleFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.DoubleFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class DoubleFieldTests extends CrudTestCase {

    @Test
    public void testNullDouble() {
        JoogarRecord.save(new DoubleFieldModel());
        DoubleFieldModel model = DoubleFieldModelTable.findById(1L);
        assertNull(model.getObjectDouble());
    }

    @Test
    public void testNullRawDouble() {
        JoogarRecord.save(new DoubleFieldModel());
        DoubleFieldModel model = DoubleFieldModelTable.findById(1L);
        assertEquals(0.0, model.getRawDouble(), 0.0000000001);
    }

    @Test
    public void testObjectDouble() {
        Double objectDouble = 25.0;
        JoogarRecord.save(new DoubleFieldModel(objectDouble));
        DoubleFieldModel model = DoubleFieldModelTable.findById(1L);
        assertEquals(objectDouble, model.getObjectDouble());
    }

    @Test
    public void testRawDouble() {
        JoogarRecord.save(new DoubleFieldModel(25.0));
        DoubleFieldModel model = DoubleFieldModelTable.findById(1L);
        assertEquals(25.0, model.getRawDouble(), 0.0000000001);
    }
}
