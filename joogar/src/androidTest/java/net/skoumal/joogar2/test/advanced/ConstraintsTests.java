package net.skoumal.joogar2.test.advanced;

import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.ConstraintIgnore;
import net.skoumal.joogar2.util.model.ConstraintIgnoreTable;
import net.skoumal.joogar2.util.model.ConstraintNonnull;
import net.skoumal.joogar2.util.model.ConstraintNonnullTable;
import net.skoumal.joogar2.util.model.ConstraintPK;
import net.skoumal.joogar2.util.model.ConstraintPKTable;
import net.skoumal.joogar2.util.model.ConstraintUnique;
import net.skoumal.joogar2.util.model.ConstraintUniqueTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class ConstraintsTests extends CrudTestCase {

    @Test
    public void testPrimaryKeyConstraint() {
        ConstraintPK entity = new ConstraintPK(1);

        JoogarRecord.insert(entity);
        assertEquals(1, ConstraintPKTable.countAll());
        assertEquals(entity.id, ConstraintPKTable.find().first().id);

        //Inserting entity with same id should fail
        entity.value = 2;
        try {
            JoogarRecord.insert(entity);
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("UNIQUE constraint failed"));
        }

        //Saving entity with same id should replace already saved one
        entity.value = 3;
        JoogarRecord.save(entity);
        assertEquals(1, ConstraintPKTable.countAll());
        assertEquals(entity.id, ConstraintPKTable.find().first().id);
        assertEquals(entity.value, ConstraintPKTable.find().first().value);

        //Updating entity with same id should update already saved one
        entity.value = 4;
        JoogarRecord.update(entity);
        assertEquals(1, ConstraintPKTable.countAll());
        assertEquals(entity.id, ConstraintPKTable.find().first().id);
        assertEquals(entity.value, ConstraintPKTable.find().first().value);

        //Updating entity with non existing id should not change it nor insert new one
        entity.id = 2;
        entity.value = 5;
        JoogarRecord.update(entity);
        assertEquals(1, ConstraintPKTable.countAll());
        assertNotEquals(entity.id, ConstraintPKTable.find().first().id);
        assertNotEquals(entity.value, ConstraintPKTable.find().first().value);
    }

    @Test
    public void testIgnoreConstraint() {
        ConstraintIgnore entity = new ConstraintIgnore();

        entity.ignoredField = 7;

        JoogarRecord.insert(entity);
        assertEquals(1, ConstraintIgnoreTable.countAll());

        //After reading from DB, ignored field should have default value (0 for int)
        entity = ConstraintIgnoreTable.findById(entity.id);
        assertEquals(0, entity.ignoredField);
    }

    @Test
    public void testNonNullConstraint() {
        ConstraintNonnull entity = new ConstraintNonnull();

        //inserting with null should fail
        try {
            JoogarRecord.insert(entity);
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("NOT NULL constraint failed"));
        }

        //saving with null should fail
        try {
            JoogarRecord.save(entity);
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("NOT NULL constraint failed"));
        }

        entity.nonnullField = 0;
        JoogarRecord.insert(entity);
        assertEquals(1, ConstraintNonnullTable.countAll());
        assertEquals(entity.id, ConstraintNonnullTable.find().first().id);

        //updating with null should fail
        entity.nonnullField = null;
        try {
            JoogarRecord.update(entity);
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("NOT NULL constraint failed"));
        }
    }

    @Test
    public void testUniqueConstraint() {
        ConstraintUnique entity = new ConstraintUnique(7);
        ConstraintUnique entity2 = new ConstraintUnique(8);

        JoogarRecord.insert(entity);
        JoogarRecord.insert(entity2);
        assertEquals(2, ConstraintUniqueTable.countAll());

        //Inserting entity with same unique field should fail
        try {
            JoogarRecord.insert(new ConstraintUnique(7));
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("UNIQUE constraint failed"));
        }

        //Saving entity with same unique field should NOT fail,
        //since save is using INSERT OR REPLACE algo
        JoogarRecord.save(new ConstraintUnique(7));
        assertEquals(2, ConstraintUniqueTable.countAll());

        //Updating entity's unique field to value which is already present in DB should fail
        entity2.uniqueField = 7;
        try {
            JoogarRecord.update(entity2);
            fail("SQLiteConstraintException should be thrown");
        } catch (SQLiteConstraintException e) {
            //desired behavior
            assertThat(e.getMessage(), containsString("UNIQUE constraint failed"));
        }
    }
}
