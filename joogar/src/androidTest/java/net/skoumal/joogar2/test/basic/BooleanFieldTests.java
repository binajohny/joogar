package net.skoumal.joogar2.test.basic;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.JoogarRecord;
import net.skoumal.joogar2.util.model.BooleanFieldModel;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.BooleanFieldModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class BooleanFieldTests extends CrudTestCase {

    @Test
    public void testNullBoolean() {
        JoogarRecord.save(new BooleanFieldModel());
        BooleanFieldModel model = BooleanFieldModelTable.findById(1L);
        assertNull(model.getObjectBoolean());
    }

    @Test
    public void testNullRawBoolean() {
        JoogarRecord.save(new BooleanFieldModel());
        BooleanFieldModel model = BooleanFieldModelTable.findById(1L);
        assertEquals(false, model.getRawBoolean());
    }

    @Test
    public void testObjectBoolean() {
        JoogarRecord.save(new BooleanFieldModel(Boolean.valueOf(true)));
        BooleanFieldModel model = BooleanFieldModelTable.findById(1L);
        assertEquals(true, model.getObjectBoolean());
    }

    @Test
    public void testRawBoolean() {
        JoogarRecord.save(new BooleanFieldModel(true));
        BooleanFieldModel model = BooleanFieldModelTable.findById(1L);
        assertEquals(true, model.getRawBoolean());
    }
}
