package net.skoumal.joogar2.test.advanced;

import android.support.test.runner.AndroidJUnit4;

import net.skoumal.joogar2.ComposedModelWhere;
import net.skoumal.joogar2.util.CrudTestCase;
import net.skoumal.joogar2.util.model.ComposedModelTable;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Pavel Salva
 */

@RunWith(AndroidJUnit4.class)
public class WhereStatementTests extends CrudTestCase {

    @Test
    public void testSimpleAnd() {

        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .and()
                .created().eq(50L)
                .and()
                .size().eq(100);



        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active = ? AND created = ? AND size = ?");
        assertEquals(args.length, 3);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "100");
    }

    @Test
    public void testSimpleOr() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .or()
                .created().eq(50L)
                .or()
                .size().eq(100);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active = ? OR created = ? OR size = ?");
        assertEquals(args.length, 3);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "100");
    }

    @Test
    public void testSimpleAndOr() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .and()
                .created().eq(50L)
                .or()
                .size().eq(100)
                .or()
                .floating().eq(2.2)
                .and()
                .id().eq(5L);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active = ? AND created = ? OR size = ? OR floating = ? AND id = ?");
        assertEquals(args.length, 5);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "100");
        assertEquals(args[3], "2.2");
        assertEquals(args[4], "5");
    }

    @Test
    public void testNestedAnd() {
        ComposedModelWhere nested = ComposedModelTable.where()
                .created().eq(3L)
                .and()
                .name().eq("");

        ComposedModelWhere nested2 = ComposedModelTable.where()
                .floating().eq(1.3)
                .and()
                .size().eq(22)
                .and(nested);

        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .and()
                .created().eq(50L)
                .and(nested2)
                .and()
                .size().eq(100);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where,
                "active = ? AND created = ? AND " +
                        "(floating = ? AND size = ? AND " +
                        "(created = ? AND name = ?)" +
                        ") AND size = ?");
        assertEquals(args.length, 7);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "1.3");
        assertEquals(args[3], "22");
        assertEquals(args[4], "3");
        assertEquals(args[5], "");
        assertEquals(args[6], "100");
    }

    @Test
    public void testNestedOr() {
        ComposedModelWhere nested = ComposedModelTable.where()
                .created().eq(3L)
                .or()
                .name().eq("");

        ComposedModelWhere nested2 = ComposedModelTable.where()
                .floating().eq(1.3)
                .or()
                .size().eq(22)
                .or(nested);

        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .or()
                .created().eq(50L)
                .or(nested2)
                .or()
                .size().eq(100);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where,
                "active = ? OR created = ? OR " +
                        "(floating = ? OR size = ? OR " +
                        "(created = ? OR name = ?)" +
                        ") OR size = ?");
        assertEquals(args.length, 7);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "1.3");
        assertEquals(args[3], "22");
        assertEquals(args[4], "3");
        assertEquals(args[5], "");
        assertEquals(args[6], "100");
    }

    @Test
    public void testNestedAndOr() {
        ComposedModelWhere nested = ComposedModelTable.where()
                .created().eq(3L)
                .and()
                .name().eq("");

        ComposedModelWhere nested2 = ComposedModelTable.where()
                .floating().eq(1.3)
                .and()
                .size().eq(22)
                .or(nested);

        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true)
                .or()
                .created().eq(50L)
                .and(nested2)
                .or()
                .size().eq(100);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where,
                "active = ? OR created = ? AND " +
                        "(floating = ? AND size = ? OR " +
                        "(created = ? AND name = ?)" +
                        ") OR size = ?");
        assertEquals(args.length, 7);
        assertEquals(args[0], "1");
        assertEquals(args[1], "50");
        assertEquals(args[2], "1.3");
        assertEquals(args[3], "22");
        assertEquals(args[4], "3");
        assertEquals(args[5], "");
        assertEquals(args[6], "100");
    }

    @Test
    public void testOperatorEq() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active = ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "1");
    }

    @Test
    public void testOperatorNonEq() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().notEq(true);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active <> ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "1");
    }

    @Test
    public void testOperatorGt() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().gt(10);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size > ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "10");
    }

    @Test
    public void testOperatorLt() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().lt(10);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size < ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "10");
    }

    @Test
    public void testOperatorGtEq() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().gtEq(10);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size >= ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "10");
    }

    @Test
    public void testOperatorLtEq() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().ltEq(10);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size <= ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "10");
    }

    @Test
    public void testOperatorBetween() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().between(10, 20);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size BETWEEN ? AND ?");
        assertEquals(args.length, 2);
        assertEquals(args[0], "10");
        assertEquals(args[1], "20");
    }

    @Test
    public void testOperatorNotBetween() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .size().not().between(10, 20);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "size NOT BETWEEN ? AND ?");
        assertEquals(args.length, 2);
        assertEquals(args[0], "10");
        assertEquals(args[1], "20");
    }

    @Test
    public void testOperatorLike() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .name().like("%name%");

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "name LIKE ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "%name%");
    }

    @Test
    public void testOperatorNotLike() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .name().not().like("%name%");

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "name NOT LIKE ?");
        assertEquals(args.length, 1);
        assertEquals(args[0], "%name%");
    }

    @Test
    public void testOperatorIsNull() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .name().isNull();

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "name IS NULL");
        assertNull(args);
    }

    @Test
    public void testOperatorIsNotNull() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .name().isNotNull();

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "name IS NOT NULL");
        assertNull(args);
    }

    @Test
    public void testOperatorIn() {
        ComposedModelWhere whereStmt;
        String where;
        String[] args;

        // one item
        whereStmt = ComposedModelTable.where()
                .name().in("A");

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name IN (?)");
        assertEquals(args.length, 1);
        assertEquals(args[0], "A");

        // two items
        whereStmt = ComposedModelTable.where()
                .name().in("A", "B");

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name IN (?,?)");
        assertEquals(args.length, 2);
        assertEquals(args[0], "A");
        assertEquals(args[1], "B");

        // many items
        whereStmt = ComposedModelTable.where()
                .name().in("A", "B", "C", "D", "E", "F");

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name IN (?,?,?,?,?,?)");
        assertEquals(args.length, 6);
        assertEquals(args[0], "A");
        assertEquals(args[1], "B");
        assertEquals(args[2], "C");
        assertEquals(args[3], "D");
        assertEquals(args[4], "E");
        assertEquals(args[5], "F");

        // collection empty
        whereStmt = ComposedModelTable.where()
                .name().in(new ArrayList<String>());

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name");
        assertNull(args);

        // collection one item
        whereStmt = ComposedModelTable.where()
                .name().in(Collections.singletonList("A"));

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name IN (?)");
        assertEquals(args.length, 1);
        assertEquals(args[0], "A");

        // collection many items
        whereStmt = ComposedModelTable.where()
                .name().in(Arrays.asList("A","B","C","D","E","F"));

        where = whereStmt.getWhere();
        args = whereStmt.getArgs();

        assertEquals(where, "name IN (?,?,?,?,?,?)");
        assertEquals(args.length, 6);
        assertEquals(args[0], "A");
        assertEquals(args[1], "B");
        assertEquals(args[2], "C");
        assertEquals(args[3], "D");
        assertEquals(args[4], "E");
        assertEquals(args[5], "F");
    }

    @Test
    public void testOperatorNotIn() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .name().not().in("A");

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "name NOT IN (?)");
        assertEquals(args.length, 1);
        assertEquals(args[0], "A");
    }

    @Test
    public void testBooleanArgs() {
        ComposedModelWhere whereStmt = ComposedModelTable.where()
                .active().eq(true).or().active().eq(false);

        String where = whereStmt.getWhere();
        String[] args = whereStmt.getArgs();

        assertEquals(where, "active = ? OR active = ?");
        assertEquals(args.length, 2);
        assertEquals(args[0], "1");
        assertEquals(args[1], "0");
    }
}
