package net.skoumal.joogar2;

import net.skoumal.joogar2.shared.util.QueryBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Jan Bína
 */

public abstract class WhereStatement<T extends WhereStatement<T>> {

    private StringBuilder whereStatement;
    private List<String> whereArgs;
    private boolean hasAndOr = true;

    protected WhereStatement() {
        this.whereStatement = new StringBuilder();
        this.whereArgs = new ArrayList<>();
    }

    protected void appendColumn(String column) {
        if (!hasAndOr) {
            and();
        } else {
            hasAndOr = false;
        }
        appendWithSpace(column);
    }

    void appendWithSpace(String str) {
        whereStatement.append(str).append(" ");
    }

    void appendWithoutSpace(String str) {
        whereStatement.append(str);
    }

    void appendArg(Object arg) {
        if (arg instanceof Boolean) {
            whereArgs.add(((Boolean) arg) ? "1" : "0");
        } else {
            whereArgs.add(String.valueOf(arg));
        }
    }

    void appendArgs(List<String> args) {
        whereArgs.addAll(args);
    }

    public String getWhere() {
        if (whereStatement == null || whereStatement.length() == 0) {
            return null;
        }
        return whereStatement.deleteCharAt(whereStatement.length() - 1).toString();
    }

    public String[] getArgs() {
        if (whereArgs == null || whereArgs.size() == 0) {
            return null;
        }
        return whereArgs.toArray(new String[whereArgs.size()]);
    }

    List<String> getArgList() {
        return whereArgs;
    }

    public T and() {
        appendWithSpace("AND");
        hasAndOr = true;
        //noinspection unchecked
        return (T) this;
    }

    public T or() {
        appendWithSpace("OR");
        hasAndOr = true;
        //noinspection unchecked
        return (T) this;
    }

    public T and(T nested) {
        appendWithoutSpace("AND (");
        appendWithoutSpace(nested.getWhere());
        appendWithSpace(")");
        appendArgs(nested.getArgList());
        //noinspection unchecked
        return (T) this;
    }

    public T or(T nested) {
        appendWithoutSpace("OR (");
        appendWithoutSpace(nested.getWhere());
        appendWithSpace(")");
        appendArgs(nested.getArgList());
        //noinspection unchecked
        return (T) this;
    }

    public static final class Comparator<T extends WhereStatement, U> {
        private T statement;

        protected Comparator(T statement) {
            this.statement = statement;
        }

        public T eq(U x) {
            statement.appendWithSpace("= ?");
            statement.appendArg(x);
            return statement;
        }

        public T notEq(U x) {
            statement.appendWithSpace("<> ?");
            statement.appendArg(x);
            return statement;
        }

        public T gt(U x) {
            statement.appendWithSpace("> ?");
            statement.appendArg(x);
            return statement;
        }

        public T lt(U x) {
            statement.appendWithSpace("< ?");
            statement.appendArg(x);
            return statement;
        }

        public T ltEq(U x) {
            statement.appendWithSpace("<= ?");
            statement.appendArg(x);
            return statement;
        }

        public T gtEq(U x) {
            statement.appendWithSpace(">= ?");
            statement.appendArg(x);
            return statement;
        }

        public T between(U x, U y) {
            statement.appendWithSpace("BETWEEN ? AND ?");
            statement.appendArg(x);
            statement.appendArg(y);
            return statement;
        }

        public T like(String pattern) {
            statement.appendWithSpace("LIKE ?");
            statement.appendArg(pattern);
            return statement;
        }

        public T isNull() {
            statement.appendWithSpace("IS NULL");
            return statement;
        }

        public T isNotNull() {
            statement.appendWithSpace("IS NOT NULL");
            return statement;
        }

        public Comparator<T, U> not() {
            statement.appendWithSpace("NOT");
            return this;
        }

        @SafeVarargs
        public final T in(U first, U... others) {
            statement.appendWithSpace("IN (" + QueryBuilder.generatePlaceholders(1 + others.length) + ")");
            statement.appendArg(first);
            for (U arg : others) {
                statement.appendArg(arg);
            }
            return statement;
        }

        public T in(Collection<U> args) {
            if (args.size() == 0) {
                return statement;
            }

            statement.appendWithSpace("IN (" + QueryBuilder.generatePlaceholders(args.size()) + ")");
            for (U arg : args) {
                statement.appendArg(arg);
            }
            return statement;
        }
    }
}
