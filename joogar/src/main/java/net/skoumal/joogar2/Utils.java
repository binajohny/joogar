package net.skoumal.joogar2;

/**
 * Created by Jan Bína
 */

public final class Utils {

    private Utils() {}

    public static byte[] toPrimitive(Byte[] array) {
        if (array == null) {
            return null;
        }

        byte[] bytes = new byte[array.length];

        for(int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                // TODO: throw NullPointerException instead?
                bytes[i] = 0;
            } else {
                bytes[i] = array[i];
            }
        }

        return bytes;
    }

    public static Byte[] toObject(byte[] array) {
        if (array == null) {
            return null;
        }

        Byte[] bytes = new Byte[array.length];

        int i = 0;
        for (byte b : array) {
            bytes[i++] = b;
        }

        return bytes;
    }
}
