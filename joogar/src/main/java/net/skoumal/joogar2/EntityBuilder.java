package net.skoumal.joogar2;

import net.skoumal.joogar2.shared.JoogarDatabaseResult;

/**
 * Created by Jan Bína
 */

public interface EntityBuilder<T> {
    T get(JoogarDatabaseResult result);
}
