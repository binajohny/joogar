package net.skoumal.joogar2;

/**
 * Created by Jan Bína
 */

public interface Transaction {
    void execute();
}
