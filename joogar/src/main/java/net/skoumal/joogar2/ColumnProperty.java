package net.skoumal.joogar2;

/**
 * Created by Jan Bína
 */

public class ColumnProperty<T> {
    private String name;

    public ColumnProperty(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }
}
