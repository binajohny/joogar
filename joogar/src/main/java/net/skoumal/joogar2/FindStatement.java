package net.skoumal.joogar2;

import net.skoumal.joogar2.shared.JoogarDatabase;
import net.skoumal.joogar2.shared.cursor.JoogarCursor;
import net.skoumal.joogar2.shared.util.QueryBuilder;

/**
 * Created by Jan Bína
 */

public class FindStatement<T, S extends WhereStatement> {

    private EntityBuilder<T> cursorFactory;
    private JoogarDatabase database;
    private String table;
    private String[] columns = null;
    private String where = null;
    private String[] whereArgs = null;
    private StringBuilder orderBy;
    private int limit = -1;
    private int offset = -1;

    public FindStatement(EntityBuilder<T> cursorFactory, JoogarDatabase database, String table,
                         String[] columns) {
        this.cursorFactory = cursorFactory;
        this.database = database;
        this.table = table;
        this.columns = columns;
        this.orderBy = new StringBuilder();
    }

    public FindStatement<T, S> where(S statement) {
        this.where = statement.getWhere();
        this.whereArgs = statement.getArgs();
        return this;
    }

    public FindStatement<T, S> limit(int limit) {
        this.limit = limit;
        return this;
    }

    public FindStatement<T, S> offset(int offset) {
        this.offset = offset;
        return this;
    }

    public FindStatement<T, S> orderBy(ColumnProperty<T> column) {
        return orderBy(column, null);
    }

    public FindStatement<T, S> orderBy(ColumnProperty<T> column, String type) {
        if (orderBy.length() != 0) {
            orderBy.append(", ");
        }
        orderBy.append(column.name());

        if (type != null) {
            if (!QueryBuilder.isValidOrderBy(type)) {
                throw new IllegalArgumentException("Invalid ORDER BY clause: [" + type + "]");
            }
            orderBy.append(" ");
            orderBy.append(type);
        }
        return this;
    }

    public JoogarCursor<T> find() {
        return database.query(table, columns, where, whereArgs, null, null, orderBy.toString(),
                buildLimitString()).toJoogarCursor(cursorFactory);
    }

    public T first() {
        this.limit = 1;

        JoogarCursor<T> cursor = find();
        T first = cursor.next();
        cursor.close();

        return first;
    }

    private String buildLimitString() {
        return offset + ", " + limit;
    }
}
