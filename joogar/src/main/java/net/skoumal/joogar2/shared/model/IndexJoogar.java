package net.skoumal.joogar2.shared.model;

/**
 * Created by Pavel Salva
 */

public class IndexJoogar {
    public String name;
    public String columnName;
    public boolean unique;
}
