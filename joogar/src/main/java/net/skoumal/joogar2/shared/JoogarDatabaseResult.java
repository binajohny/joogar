package net.skoumal.joogar2.shared;

import net.skoumal.joogar2.EntityBuilder;
import net.skoumal.joogar2.shared.cursor.JoogarCursor;

import java.io.Closeable;

/**
 * Created by Pavel Salva
 */
public abstract class JoogarDatabaseResult implements Closeable {

    public abstract boolean next();

    @Override
    public abstract void close();

    public abstract int count();

    public abstract int getPosition();

    public abstract int getColumnIndex(String columnName);

    public abstract int getColumnIndexOrThrow(String columnName);

    public abstract boolean isNull(int columnIndex);

    public abstract long getLong(int columnIndex);

    public abstract String getString(int columnIndex);

    public abstract double getDouble(int columnIndex);

    public abstract byte[] getBlob(int columnIndex);

    public abstract int getInt(int columnIndex);

    public abstract float getFloat(int columnIndex);

    public abstract void setPosition(int position);

    public abstract boolean allowsRandomAccess();

    public abstract <T> JoogarCursor<T> toJoogarCursor(EntityBuilder<T> entityBuilder);

    public boolean hasColumn(String columnName) {
        return getColumnIndex(columnName) >= 0;
    }
}
