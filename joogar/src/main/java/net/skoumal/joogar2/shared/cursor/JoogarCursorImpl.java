package net.skoumal.joogar2.shared.cursor;

import net.skoumal.joogar2.EntityBuilder;
import net.skoumal.joogar2.shared.JoogarDatabaseResult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jan Bína
 */

public class JoogarCursorImpl<T> implements JoogarCursor<T>, Iterator<T> {

    private EntityBuilder<T> factory;
    private JoogarDatabaseResult result;
    private boolean isClosed = false;

    public JoogarCursorImpl(EntityBuilder<T> factory, JoogarDatabaseResult result) {
        this.factory = factory;
        this.result = result;
    }

    /**
     * Returns the numbers of rows in joogar cursor.
     *
     * @return the number of rows in the cursor
     */
    public int getCount() {
        return size();
    }

    public int size() {
        return result.count();
    }

    /**
     * Returns the current position of the cursor in the row set.
     * The value is zero-based. Initially the cursor is at position -1,
     * which is before the first row. After the
     * last row is returned another call to next() will leave the cursor past
     * the last entry, at a position of count().
     *
     * @return the current cursor position.
     */
    public int getPosition() {
        return result.getPosition();
    }

    /**
     * Moves the cursor to given position. Allowed values are from 0 to count.
     *
     * @param position the zero-based position to move to
     */
    public void setPosition(int position) {
        result.setPosition(position);
    }

    @Override
    public boolean hasNext() {
        return result.getPosition() < (result.count() - 1);
    }

    /**
     * Move the cursor to the next item.
     *
     * @return next item
     */
    public T next() {
        if (result.next()) {
            return getItem();
        } else {
            return null;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is not supported for joogar cursor iterator.");
    }

    /**
     * Closes the Cursor, releasing all of its underlining resources.
     */
    public void close() {
        isClosed = true;
        result.close();
    }

    /**
     * Return true when cursor is close.
     *
     * @return true if {@link #close()} method was called
     */
    public boolean isClosed() {
        return isClosed;
    }

    /**
     * Returns row as item object.
     *
     * @return data object
     */
    public T getItem() {
        return current();
    }

    public T current() {
        return factory.get(result);
    }

    @Override
    public List<T> toList() {
        List<T> list = new ArrayList<>(size());
        setPosition(-1);
        T item;
        while ((item = next()) != null) {
            list.add(item);
        }
        return list;
    }

    @Override
    public List<T> toListAndClose() {
        List<T> list = toList();
        close();
        return list;
    }

    @Override
    public Iterator<T> iterator() {
        setPosition(-1); // reset position pointer
        return this;
    }
}
