package net.skoumal.joogar2.shared;


import java.io.File;

/**
 * Created by Joogar
 */
public interface JoogarObjectAbstractFactory {

    JoogarLogger getLogger();

    JoogarDatabase getDatabase(String gName, boolean gWalMode);

    JoogarDatabase getDatabase(File gPath, boolean gWalMode);
}
