/*
 * Copyright 2015 SKOUMAL, s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.skoumal.joogar2.shared;

import android.content.ContentValues;
import android.content.Context;

import net.skoumal.joogar2.android.AndroidObjectFactory;
import net.skoumal.joogar2.shared.model.ColumnJoogar;
import net.skoumal.joogar2.shared.model.DatabaseJoogar;
import net.skoumal.joogar2.shared.model.TableJoogar;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Pavel Salva
 */

public abstract class JoogarBase {

    private static JoogarBase instance;
    private static boolean isDebug;

    private JoogarObjectAbstractFactory objectFactory;
    private Hashtable<String, JoogarDatabase> nameToDatabase = new Hashtable<>();
    private Hashtable<String, JoogarDatabase> entityToDatabase = new Hashtable<>();

    protected JoogarBase() {
        instance = this;
    }

    public static JoogarBase getInstance() {
        return instance;
    }

    public JoogarDatabase getDB(String dbName) {
        return nameToDatabase.get(dbName);
    }

    public JoogarDatabase getDBforTable(String tableName) {
        return entityToDatabase.get(tableName);
    }

    public JoogarLogger getLogger() {
        return objectFactory.getLogger();
    }

    public static boolean isDebug() {
        return isDebug;
    }

    public static void setDebug(boolean debug) {
        isDebug = debug;
    }

    public List<JoogarDatabase> getDatabases() {
        return new ArrayList<>(nameToDatabase.values());
    }

    protected void close() {
        for (JoogarDatabase database : getDatabases()) {
            database.close();
        }
        instance = null;
    }

    protected void baseInit(Context context) {
        objectFactory = new AndroidObjectFactory(context);
    }

    protected void addDB(JoogarDatabaseBuilder gBuilder) {
        if (nameToDatabase.get(gBuilder.getName()) != null) {
            throw new IllegalArgumentException("Database with name " + gBuilder.getName() + " already exists.");
        }

        // create database
        //TODO [3] implement attach database functionality:
        // https://www.sqlite.org/lang_attach.html
        // attaching should be preferred way to add more databases
        // be careful with WAL mode, since it broke atomicity of commits as mentioned on the link above
        JoogarDatabase database = gBuilder.build(objectFactory);

        // keep reference to database
        nameToDatabase.put(gBuilder.getName(), database);

        // assign entities to database
        List<TableJoogar> domainClasses = gBuilder.getDatabaseSchema().getTables();
        for (TableJoogar c : domainClasses) {
            JoogarDatabase oldValue = entityToDatabase.put(c.getName(), database);

            if (oldValue != null) {
                throw new IllegalArgumentException("You are trying to assign entity '" + c.getName() +
                        "' to more than one database. Use different sublasses if you need the " +
                        "same entity stored in more databases.");
            }
        }
    }

    protected void setDatabaseCustomMigrations(String[] migrationsArray, String databaseName) {
        int version = getCustomVersionForDatabase(databaseName);

        if (migrationsArray == null || migrationsArray.length <= version) {
            return;
        }

        JoogarDatabase db = getDB(databaseName);
        for (int i = version; i < migrationsArray.length; ++i) {
            db.execSQL(migrationsArray[i]);
        }

        setCustomVersionForDatabase(databaseName, migrationsArray.length);

    }

    protected void createDatabases() {
        createJoogarSettingsDatabase();
    }

    private void createJoogarSettingsDatabase() {
        DatabaseJoogar database = new DatabaseJoogar(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB);
        TableJoogar entity;
        entity = new TableJoogar("database_hash", "DatabaseHash", null, 1);
        entity.addColumn(new ColumnJoogar("database_name", true, false, false, "text", false));
        entity.addColumn(new ColumnJoogar("hash", false, false, false, "text", false));
        entity.addColumn(new ColumnJoogar("version", false, false, true, "integer", false));
        database.addTable(entity);
        JoogarDatabaseBuilder builder = new JoogarDatabaseBuilder().setDatabaseSchema(database);
        addDB(builder);
    }

    private int getCustomVersionForDatabase(String databaseName) {
        JoogarDatabase db = getDB(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB);
        JoogarDatabaseResult result = db.rawQuery("SELECT version FROM database_hash WHERE database_name=?", new String[]{databaseName});
        if (result.next()) {
            return result.getInt(result.getColumnIndex("version"));
        } else {
            return 0;
        }
    }

    private void setCustomVersionForDatabase(String databaseName, int version) {
        JoogarDatabase db = getDB(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB);
        ContentValues values = new ContentValues();
        values.put("version", version);

        db.insertWithOnConflict("database_hash", null, values, JoogarDatabase.CONFLICT_REPLACE);
        db.update("database_hash", values, "database_name = ?", new String[]{databaseName});
    }
}
