package net.skoumal.joogar2.shared;

import android.content.ContentValues;
import android.database.sqlite.SQLiteStatement;

import net.skoumal.joogar2.Transaction;
import net.skoumal.joogar2.shared.util.QueryBuilder;

import java.io.File;

/**
 * Created by Joogar
 */
public abstract class JoogarDatabase {

    /**
     * When a constraint violation occurs, an immediate ROLLBACK occurs,
     * thus ending the current transaction, and the command aborts with a
     * return code of SQLITE_CONSTRAINT. If no transaction is active
     * (other than the implied transaction that is created on every command)
     * then this algorithm works the same as ABORT.
     */
    public static final int CONFLICT_ROLLBACK = 1;

    /**
     * When a constraint violation occurs,no ROLLBACK is executed
     * so changes from prior commands within the same transaction
     * are preserved. This is the default behavior.
     */
    public static final int CONFLICT_ABORT = 2;

    /**
     * When a constraint violation occurs, the command aborts with a return
     * code SQLITE_CONSTRAINT. But any changes to the database that
     * the command made prior to encountering the constraint violation
     * are preserved and are not backed out.
     */
    public static final int CONFLICT_FAIL = 3;

    /**
     * When a constraint violation occurs, the one row that contains
     * the constraint violation is not inserted or changed.
     * But the command continues executing normally. Other rows before and
     * after the row that contained the constraint violation continue to be
     * inserted or updated normally. No error is returned.
     */
    public static final int CONFLICT_IGNORE = 4;

    /**
     * When a UNIQUE constraint violation occurs, the pre-existing rows that
     * are causing the constraint violation are removed prior to inserting
     * or updating the current row. Thus the insert or update always occurs.
     * The command continues executing normally. No error is returned.
     * If a NOT NULL constraint violation occurs, the NULL value is replaced
     * by the default value for that column. If the column has no default
     * value, then the ABORT algorithm is used. If a CHECK constraint
     * violation occurs then the IGNORE algorithm is used. When this conflict
     * resolution strategy deletes rows in order to satisfy a constraint,
     * it does not invoke delete triggers on those rows.
     * This behavior might change in a future release.
     */
    public static final int CONFLICT_REPLACE = 5;

    /**
     * Use the following when no conflict action is specified.
     */
    public static final int CONFLICT_NONE = 0;

    private File path;

    protected boolean walMode;

    /**
     * Create new database object.
     * @param gPath path to database file
     */
    public JoogarDatabase(File gPath, boolean gWalMode) {
        path = gPath;
        walMode = gWalMode;
    }

    public long insert(String table, String nullColumnHack, ContentValues initialValues) {
        return insertWithOnConflict(table, nullColumnHack, initialValues, CONFLICT_NONE);
    }

    public abstract long insertWithOnConflict(String table, String nullColumnHack,
                                              ContentValues initialValues, int conflictAlgorithm);

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return updateWithOnConflict(table, values, whereClause, whereArgs, CONFLICT_NONE);
    }

    public abstract int updateWithOnConflict(String table, ContentValues values,
                                             String whereClause, String[] whereArgs, int conflictAlgorithm);

    public abstract int delete(String table, String whereClause, String[] whereArgs);

    public JoogarDatabaseResult query(String table, String[] columns, String selection,
                                      String[] selectionArgs, String groupBy, String having,
                                      String orderBy) {
        return query(table, columns, selection, selectionArgs, groupBy, having, orderBy, null);
    }

    public JoogarDatabaseResult query(String table, String[] columns, String selection,
                                      String[] selectionArgs, String groupBy, String having,
                                      String orderBy, String limit) {
        return query(false, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public JoogarDatabaseResult query(boolean distinct, String table, String[] columns,
                                      String selection, String[] selectionArgs,
                                      String groupBy, String having, String orderBy,
                                      String limit) {
        String query = QueryBuilder.buildQueryString(distinct, table, columns,
                selection, groupBy, having, orderBy, limit);
        return rawQuery(query, selectionArgs);
    }

    public abstract JoogarDatabaseResult rawQuery(String query, String[] arguments);

    public abstract void execSQL(String query);

    public abstract void execSQL(String query, String[] arguments);

    public abstract SQLiteStatement compileStatement(String statement);

    public abstract void beginTransaction();

    public abstract void setTransactionSuccessful();

    public abstract void endTransaction();

    public abstract void rollbackTransaction();

    public void executeTransaction(Transaction transaction) {
        beginTransaction();
        try {
            transaction.execute();
            setTransactionSuccessful();
        } finally {
            endTransaction();
        }
    }

    public abstract void close();

    public File getPath() {
        return path;
    }

    /**
     * Get current database version.
     *
     * @return current database version
     */
    public int getVersion() {
        JoogarDatabaseResult result = rawQuery("PRAGMA user_version;", null);
        result.next();
        int version = result.getInt(0);
        result.close();
        return version;
    }

    /**
     * Set database version. Used for upgrading database schema.
     *
     * @param gVersion desired version
     */
    void setVersion(int gVersion) {
        execSQL("PRAGMA user_version = " + gVersion);
    }
}
