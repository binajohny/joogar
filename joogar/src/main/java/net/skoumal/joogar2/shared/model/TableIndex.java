package net.skoumal.joogar2.shared.model;

/**
 * Created by Pavel Salva
 */

public class TableIndex {

    private String [] columns;
    private boolean unique = false;

    public TableIndex(String [] gColumns, boolean gUnique) {
        columns = gColumns;
        unique = gUnique;
    }

    public String[] columns() {
        return columns;
    }

    public boolean unique() {
        return unique;
    }
}
