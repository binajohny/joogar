package net.skoumal.joogar2.shared;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Pavel Salva
 */
public interface SystemUtils {

    /**
     * Returns list of upgrade script names (01.sql, 02.sql, ...).
     * @param gDbName database name
     * @return list of scripts
     */
    public List<String> getUpgradeScripts(String gDbName);

    public InputStream openUpgradeScript(String gScriptName);
}
