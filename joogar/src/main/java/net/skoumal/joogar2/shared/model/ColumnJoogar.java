package net.skoumal.joogar2.shared.model;

/**
 * Created by Pavel Salva
 */

public class ColumnJoogar {

    private String name;
    private boolean primaryKey;
    private boolean unique;
    private boolean nonNull;
    private String sqlType;
    private boolean index;

    public ColumnJoogar(String gName, boolean gPrimaryKey, boolean gUnique, boolean gNonNull, String gSqlType, boolean gIndex) {
        name = gName;
        primaryKey = gPrimaryKey;
        unique = gUnique;
        nonNull = gNonNull;
        sqlType = gSqlType;
        index = gIndex;
    }


    public boolean isNonNull() {
        return nonNull;
    }

    public void setNonNull(boolean nonNull) {
        this.nonNull = nonNull;
    }

    public String getSqlType() {
        return sqlType;
    }

    public void setSqlType(String sqlType) {
        this.sqlType = sqlType;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TableIndex getTableIndex() {
        if (index) {
            return new TableIndex(new String[]{name}, false);
        } else {
            return null;
        }
    }

    public boolean isIndex() {
        return index;
    }
}
