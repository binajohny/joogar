package net.skoumal.joogar2.shared.util;

import android.content.ContentValues;
import android.text.TextUtils;
import android.util.Log;

import net.skoumal.joogar2.shared.JoogarBase;
import net.skoumal.joogar2.shared.JoogarDatabase;
import net.skoumal.joogar2.shared.JoogarDatabaseBuilder;
import net.skoumal.joogar2.shared.JoogarDatabaseResult;
import net.skoumal.joogar2.shared.model.ColumnJoogar;
import net.skoumal.joogar2.shared.model.DatabaseJoogar;
import net.skoumal.joogar2.shared.model.IndexJoogar;
import net.skoumal.joogar2.shared.model.TableIndex;
import net.skoumal.joogar2.shared.model.TableJoogar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Pavel Salva
 */

public class SchemaGenerator {

    private JoogarDatabase database;

    private String databaseName;

    public SchemaGenerator(JoogarDatabase gDatabase, String gDbName) {
        database = gDatabase;
        databaseName = gDbName;
    }


    public void doUpgrade(DatabaseJoogar databaseJoogar, String oldHash) {
        String sql = "select count(*) from sqlite_master where type='table' and name LIKE '%s';";
        for (TableJoogar domain : databaseJoogar.getTables()) {
            JoogarDatabaseResult result = database.rawQuery(String.format(sql, domain.getName()), null);
            if (result.next() && result.getInt(0) == 0) {
                if(domain.getName().equals("integerFieldModel")){
                    Log.d("test","test");
                }
                createTable(domain);
            } else {
                upgradeTable(domain);
            }
            //TODO: indexes
            upgradeTableIndexes(domain);

            result.close();
        }

        if(!databaseJoogar.getName().equals(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB)) {
            if(!TextUtils.isEmpty(oldHash)) {
                setHashForDatabase(databaseJoogar.getName(), databaseJoogar.getHash());
            }else{
                setHashAndVersionForDatabase(databaseJoogar.getName(), databaseJoogar.getHash(), 0);
            }
        }
    }

    private void upgradeTable(TableJoogar table) {

        if (JoogarBase.isDebug()) {
            JoogarBase.getInstance().getLogger().i("Upgrade table");
        }

        // fields
        List<ColumnJoogar> columnsNew = table.getColumns();
        String tableName = table.getName();

        List<ColumnJoogar> columnsOld = getTableSchema(database, tableName);

        for (ColumnJoogar columnJoogar : columnsNew) {
            String columnName = columnJoogar.getName();

            ColumnJoogar columnInOld = null;
            ColumnJoogar columnInNew = null;
            for (ColumnJoogar column : columnsOld) {
                String columnName1 = column.getName().toLowerCase(Locale.UK);
                String columnName2 = columnName.toLowerCase(Locale.UK);
                if (columnName1.equals(columnName2)) {
                    columnInOld = column;
                    columnInNew = columnJoogar;
                    break;
                }
            }

            if (columnInOld == null) {
                createTableColumn(table, columnJoogar);
            } else {
                upgradeTableColumn(table, columnInNew, columnInOld);
            }
        }

    }

    private void upgradeTableIndexes(TableJoogar table) {
        String tableName = table.getName();
        List<TableIndex> classIndexes = table.getTableIndexes();
        List<IndexJoogar> tableIndexes = getTableIndexes(database, tableName);

        for (TableIndex ti : classIndexes) {
            String indexName = NamingHelper.toSQLName(ti, table);

            boolean found = false;
            for (int i = 0; i < tableIndexes.size(); i++) {
                if (TextUtils.equals(indexName, tableIndexes.get(i).name)) {
                    tableIndexes.remove(i);
                    found = true;
                    break;
                }
            }

            if (!found) {
                createTableIndex(ti, table);
            }
        }

        for (IndexJoogar dbIndex : tableIndexes) {
            if (dbIndex.name.startsWith("joogar_idx_")) {
                deleteTableIndex(dbIndex);
            }
        }
    }

    private void deleteTableIndex(IndexJoogar gIndex) {
        StringBuilder sb = new StringBuilder("DROP ");
        sb.append("INDEX \"");
        sb.append(gIndex.name);
        sb.append("\"");

        database.execSQL(sb.toString());
    }

    private void createTableIndex(TableIndex gIndex, TableJoogar gTable) {
        String indexName = NamingHelper.toSQLName(gIndex, gTable);

        StringBuilder sb = new StringBuilder("CREATE ");
        if (gIndex.unique()) {
            sb.append("UNIQUE ");
        }
        sb.append("INDEX \"");
        sb.append(indexName).append("\" ");
        sb.append("ON \"");
        sb.append(gTable.getName()).append("\" (");

        boolean first = true;
        for (String column : gIndex.columns()) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }

            sb.append(column);
        }

        sb.append(")");

        database.execSQL(sb.toString());
    }


    private void createTableColumn(TableJoogar table, ColumnJoogar gField) {
        String tableName = table.getName();

        StringBuilder sb = new StringBuilder("ALTER TABLE \"");
        sb.append(tableName).append("\" ");

        String columnDeclaration = createSqlColumnDeclaration(gField, true);
        sb.append("ADD COLUMN ").append(columnDeclaration);

        database.execSQL(sb.toString());

    }

    private void upgradeTableColumn(TableJoogar gTable, ColumnJoogar gColumnNew, ColumnJoogar gColumnOld) {
        //TODO: now we have all info fot upgrading column :)
    }


    private void createTable(TableJoogar table) {
        List<ColumnJoogar> fields = table.getColumns();
        String tableName = table.getName();
        int noOfPks = table.getNoOfPks();
        StringBuilder sb = new StringBuilder("CREATE TABLE \"");
        sb.append(tableName).append("\" ( ");

        for (int i = 0; i < fields.size(); ++i) {
            ColumnJoogar column = fields.get(i);
            String columnType = column.getSqlType();

            if (columnType != null) {
                String columnDeclaration = createSqlColumnDeclaration(column, noOfPks == 1);

                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(columnDeclaration);
            }
        }

        //Composite primary key creation
        //TODO: Do we want to automatically set PK column as NON NULL? (http://stackoverflow.com/a/3685672/2801938)
        if (noOfPks > 1) {
            sb.append(", PRIMARY KEY (");
            for (int i = 0; i < fields.size(); ++i) {
                ColumnJoogar column = fields.get(i);
                String columnType = column.getSqlType();

                if (columnType != null) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    sb.append(column.getName());
                }
            }
            sb.append(")");
        }

        sb.append(" ) ");

        if (JoogarBase.isDebug()){
            JoogarBase.getInstance().getLogger().i("Creating table " + tableName);
        }

        String statement = sb.toString();
        JoogarBase.getInstance().getLogger().w(statement);
        if (!TextUtils.isEmpty(statement)){
            database.execSQL(sb.toString());
        }
    }

    private String createSqlColumnDeclaration(ColumnJoogar column, boolean appendPrimaryKey) {

        StringBuilder sb = new StringBuilder();


        sb.append("\"").append(column.getName()).append("\" ").append(" ").append(column.getSqlType());

        if (column.isPrimaryKey() && appendPrimaryKey) {
            if(column.getSqlType().equalsIgnoreCase("integer")) {
                sb.append(" PRIMARY KEY AUTOINCREMENT");
            }else{
                sb.append(" PRIMARY KEY");
            }
        }

        if (column.isNonNull()) {
            sb.append(" NOT NULL");
        }

        if (column.isUnique()) {
            sb.append(" UNIQUE");
        }

        return sb.toString();
    }

    private List<ColumnJoogar> getTableSchema(JoogarDatabase db, String tableName) {

        JoogarDatabaseResult result = db.rawQuery("PRAGMA table_info (\"" + tableName + "\")", null);
        List<ColumnJoogar> columns = new ArrayList<>();
        int nameCol = result.getColumnIndex("name");
        int typeCol = result.getColumnIndex("type");
        int notNullCol = result.getColumnIndex("notnull");
        int pkCol = result.getColumnIndex("pk");

        List<IndexJoogar> indexes = getTableIndexes(db, tableName);

        while (result.next()) {
            String columnName = result.getString(nameCol);
            ColumnJoogar column = new ColumnJoogar(
                    columnName,
                    result.getInt(pkCol) > 0,
                    isColumnUnique(indexes, columnName),
                    result.getInt(notNullCol) > 0,
                    result.getString(typeCol),
                    false
            );
            columns.add(column);
        }

        return columns;
    }

    private boolean isColumnUnique(List<IndexJoogar> indexes, String columnName) {

        for (IndexJoogar index : indexes) {
            if (index.columnName.equals(columnName)) {
                return true;
            }
        }

        return false;
    }

    private List<IndexJoogar> getTableIndexes(JoogarDatabase db, String tableName) {

        List<IndexJoogar> indexes = new ArrayList<>();
        JoogarDatabaseResult resultIndexes = db.rawQuery("PRAGMA INDEX_LIST (\"" + tableName + "\")", null);
        int indexNameCol = resultIndexes.getColumnIndex("name");
        int indexUniqueCol = resultIndexes.getColumnIndex("unique");

        while (resultIndexes.next()) {
            IndexJoogar index = new IndexJoogar();
            index.name = resultIndexes.getString(indexNameCol);
            index.unique = resultIndexes.getInt(indexUniqueCol) > 0;
            JoogarDatabaseResult resultCurrentIndex = db.rawQuery("PRAGMA index_info(\"" + index.name + "\")", null);
            resultCurrentIndex.next();
            index.columnName = resultCurrentIndex.getString(resultCurrentIndex.getColumnIndex("name"));
            indexes.add(index);
        }

        return indexes;
    }

    private void setHashForDatabase(String databaseName, String hash) {
        JoogarDatabase db = JoogarBase.getInstance().getDB(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB);
        ContentValues values = new ContentValues();
        values.put("hash", hash);
        db.update("database_hash", values, "database_name = ?", new String[]{databaseName});
    }

    private void setHashAndVersionForDatabase(String databaseName, String hash, int version) {
        JoogarDatabase db = JoogarBase.getInstance().getDB(JoogarDatabaseBuilder.JOOGAR_SETTINGS_DB);
        ContentValues values = new ContentValues();
        values.put("database_name", databaseName);
        values.put("hash", hash);
        values.put("version", version);
        db.insertWithOnConflict("database_hash", null, values, JoogarDatabase.CONFLICT_REPLACE);
    }

}
