package net.skoumal.joogar2.shared.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Salva
 */

public class DatabaseJoogar {
    private List<TableJoogar> tables;
    private String databaseName;
    private String hash;

    public DatabaseJoogar(String gDatabaseName){
        tables = new ArrayList<>();
        databaseName = gDatabaseName;
    }

    public List<TableJoogar> getTables(){
        return tables;
    }

    public void addTable(TableJoogar gTable){
        tables.add(gTable);
    }

    public String getName() {
        return databaseName;
    }

    public void setName(String name) {
        this.databaseName = name;
    }

    public void addHash(String gHash){
        hash = gHash;
    }

    public String getHash() {
        return hash;
    }
}
