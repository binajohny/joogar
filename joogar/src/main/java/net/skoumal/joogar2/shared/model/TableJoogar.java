package net.skoumal.joogar2.shared.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Salva
 */

public class TableJoogar {
    private List<ColumnJoogar> columns;
    private String name;
    private String entityName;
    private List<TableIndex> tableIndexes;
    private int noOfPks;

    public TableJoogar(String name, String entityName, List<TableIndex> tableIndexes, int noOfPks) {
        this.columns = new ArrayList<>();
        this.name = name;
        this.entityName = entityName;
        if (tableIndexes == null) {
            this.tableIndexes = new ArrayList<>();
        } else {
            this.tableIndexes = tableIndexes;
        }
        this.noOfPks = noOfPks;
    }

    public List<ColumnJoogar> getColumns() {
        return columns;
    }

    public void addColumn(ColumnJoogar gColumn) {
        if (gColumn.isIndex()) {
            tableIndexes.add(gColumn.getTableIndex());
        }
        columns.add(gColumn);
    }

    public String getName() {
        return name;
    }

    public List<TableIndex> getTableIndexes() {
        return tableIndexes;
    }

    public int getNoOfPks() {
        return noOfPks;
    }
}
