package net.skoumal.joogar2.shared;

/**
 * Created by Joogar
 */
public interface JoogarLogger {

    /**
     * Prints error message
     * @param gMsg message
     */
    void e(String gMsg);

    /**
     * Prints warning message
     * @param gMsg message
     */
    void w(String gMsg);

    /**
     * Prints info message
     * @param gMsg message
     */
    void i(String gMsg);

    /**
     * Prints debug message
     * @param gMsg message
     */
    void d(String gMsg);
}
