package net.skoumal.joogar2.shared;

import android.text.TextUtils;

import net.skoumal.joogar2.shared.model.DatabaseJoogar;
import net.skoumal.joogar2.shared.util.SchemaGenerator;

import java.io.File;
import java.security.InvalidParameterException;

/**
 * Created by Pavel Salva
 */
public class JoogarDatabaseBuilder {
    public static final String JOOGAR_SETTINGS_DB = "joogar_settings";

    private File dbPath;
    private String dbName = "joogar";
    private DatabaseJoogar databaseJoogar;

    /**
     * Enables write-ahead-log mode. Enabled by default because of better performance in most of
     * use-cases.
     */
    private boolean walMode = true;


    /**
     * Defines particular db file to open.
     *
     * @param path path to your database
     */
    public JoogarDatabaseBuilder setFile(File path) {
        this.dbPath = path;
        return this;
    }


    public String getName() {
        return databaseJoogar.getName();
    }

    public JoogarDatabaseBuilder setDatabaseSchema(DatabaseJoogar gDatabase) {
        if (!TextUtils.isEmpty(gDatabase.getName())) {
            dbName = gDatabase.getName();
        }
        databaseJoogar = gDatabase;
        return this;
    }


    /**
     * Enables write-ahead-log (WAL) mode. Cannot be enabled for API levels below 11, for those will be
     * silently ignored. For Android API level 11 and higher and for iOS is WAL enabled by default
     * because of better performance in most of use-cases and ability to read while performing
     * transaction in the other thread.
     *
     * @param gWalMode true to enable write-ahead-log mode
     */
    public JoogarDatabaseBuilder setWalMode(boolean gWalMode) {
        walMode = gWalMode;

        return this;
    }

    public boolean isWalMode() {
        return walMode;
    }

    public JoogarDatabase build(JoogarObjectAbstractFactory gObjectFactory) {
        JoogarDatabase db = createDatabase(gObjectFactory);

        int currentVersion = db.getVersion();


        SchemaGenerator schemaGenerator = new SchemaGenerator(db, dbName);


        String oldHash = null;
        if (!databaseJoogar.getName().equals(JOOGAR_SETTINGS_DB)) {
            oldHash = getHashForDatabase(dbName);
        }

        if ((oldHash == null) || !oldHash.equals(databaseJoogar.getHash())) {
            schemaGenerator.doUpgrade(databaseJoogar, oldHash);
            db.setVersion(currentVersion + 1);
        }


        return db;
    }

    private JoogarDatabase createDatabase(JoogarObjectAbstractFactory gObjectFactory) {
        JoogarDatabase db;
        if (dbPath != null) {
            db = gObjectFactory.getDatabase(dbPath, walMode);
        } else if (dbName != null) {
            db = gObjectFactory.getDatabase(dbName, walMode);
        } else {
            throw new InvalidParameterException("Please define database path or name.");
        }
        return db;
    }

    public DatabaseJoogar getDatabaseSchema() {
        return databaseJoogar;
    }

    private String getHashForDatabase(String databaseName) {
        JoogarDatabase db = JoogarBase.getInstance().getDB(JOOGAR_SETTINGS_DB);
        JoogarDatabaseResult result = db.rawQuery("SELECT hash FROM database_hash WHERE database_name=?", new String[]{databaseName});
        if (result.next()) {
            return result.getString(result.getColumnIndex("hash"));
        } else {
            return null;
        }
    }
}
