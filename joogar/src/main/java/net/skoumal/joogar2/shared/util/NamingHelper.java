package net.skoumal.joogar2.shared.util;

import net.skoumal.joogar2.shared.model.TableIndex;
import net.skoumal.joogar2.shared.model.TableJoogar;

/**
 * Created by Pavel Salva
 */

public class NamingHelper {

    public static String toSQLName(TableIndex gIndex, TableJoogar gTable) {
        StringBuilder indexName = new StringBuilder();
        indexName.append("joogar_idx");
        if(gIndex.unique()) {
            indexName.append("_unq");
        }
        indexName.append(gTable.getName());
        for (String c : gIndex.columns()){
            indexName.append("_");
            indexName.append(c);
        }

        return indexName.toString();
    }

}
