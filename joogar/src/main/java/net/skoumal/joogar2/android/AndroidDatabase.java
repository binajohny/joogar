/*
 * Copyright 2015 SKOUMAL, s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.skoumal.joogar2.android;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;

import net.skoumal.joogar2.android.util.JoogarCursorFactory;
import net.skoumal.joogar2.shared.JoogarBase;
import net.skoumal.joogar2.shared.JoogarDatabase;
import net.skoumal.joogar2.shared.JoogarDatabaseResult;

import java.io.File;

/**
 * Created by Joogar
 */

class AndroidDatabase extends JoogarDatabase {

    private static final boolean WAL_SUPPORTED = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;

    private SQLiteDatabase db;

    AndroidDatabase(File gPath, boolean gWalMode) {
        super(gPath, gWalMode && WAL_SUPPORTED);

        openDatabase(gPath, gWalMode && WAL_SUPPORTED);
    }

    AndroidDatabase(String gName, Context gContext, boolean gWalMode) {
        super(gContext.getDatabasePath(gName), gWalMode && WAL_SUPPORTED);

        File path = getPath();
        File directory = path.getParentFile();

        if(!directory.exists()) {
            directory.mkdirs();
        }

        openDatabase(path, gWalMode && WAL_SUPPORTED);
    }

    @Override
    public long insertWithOnConflict(String table, String nullColumnHack, ContentValues initialValues,
                                     int conflictAlgorithm) {
        return db.insertWithOnConflict(table, nullColumnHack, initialValues, conflictAlgorithm);
    }

    @Override
    public int updateWithOnConflict(String table, ContentValues values,
                                    String whereClause, String[] whereArgs, int conflictAlgorithm) {
        return db.updateWithOnConflict(table, values, whereClause, whereArgs, conflictAlgorithm);
    }

    @Override
    public int delete(String table, String whereClause, String[] whereArgs) {
        return db.delete(table, whereClause, whereArgs);
    }

    @Override
    @SuppressLint("Recycle")
    public JoogarDatabaseResult rawQuery(String query, String[] arguments)  {
        Cursor cursor = db.rawQuery(query, arguments);
        return new AndroidDatabaseResult(cursor);
    }

    @Override
    public void execSQL(String query)  {
        db.execSQL(query);
    }

    @Override
    public void execSQL(String query, String[] arguments)  {
        db.execSQL(query, arguments);
    }

    @Override
    public SQLiteStatement compileStatement(String statement) {
        return db.compileStatement(statement);
    }

    @Override
    public void beginTransaction() {
        if(walMode) { // immediate transaction in WAL mode allows parallel reads and write
            db.beginTransactionNonExclusive();
        } else {
            db.beginTransaction();
        }
    }

    @Override
    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    @Override
    public void endTransaction() {
        db.endTransaction();
    }

    @Override
    public void rollbackTransaction() {
        db.endTransaction();
    }

    @Override
    public void close() {
        if(db.isOpen()) {
            db.close();
        }
    }

    private void openDatabase(File gPath, boolean gWalMode) {
        int flags = SQLiteDatabase.CREATE_IF_NECESSARY;
        if(gWalMode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                flags = flags | SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING;
            }
        }
        db = SQLiteDatabase.openDatabase(gPath.getPath(), new JoogarCursorFactory(), flags);

        // backward compatibility hack to support WAL on pre-jelly-bean devices
        if(gWalMode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
                    Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                db.enableWriteAheadLogging();
            } else if (!WAL_SUPPORTED) {
                JoogarBase.getInstance().getLogger().w("WAL is not supported on API levels below 11.");
            }
        }
    }
}
