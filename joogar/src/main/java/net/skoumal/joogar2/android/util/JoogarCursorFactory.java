package net.skoumal.joogar2.android.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

import net.skoumal.joogar2.shared.JoogarBase;

/**
 * Created by Joogar
 */

public class JoogarCursorFactory implements SQLiteDatabase.CursorFactory {
    @Override
    public Cursor newCursor(SQLiteDatabase sqLiteDatabase,
                            SQLiteCursorDriver sqLiteCursorDriver,
                            String editTable,
                            SQLiteQuery sqLiteQuery) {

        if(JoogarBase.isDebug()) {
            JoogarBase.getInstance().getLogger().d(sqLiteQuery.toString());
        }

        return new SQLiteCursor(sqLiteCursorDriver, editTable, sqLiteQuery);
    }
}
